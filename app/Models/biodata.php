<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class biodata extends Model
{
    use HasFactory;

    protected $table = 'members';
    protected $fillable = [
        'nama',
        'username',
        'email',
        'password', // Ingat untuk menggunakan cara yang aman untuk menyimpan password
        'provinsi',
        'kota',
        'kecamatan',
        'alamat_detail',
        'no_hp',
    ];
    public function member()
    {
        return $this->belongsTo(Member::class, 'id');
    }
}