<?php
// app/Models/Barang.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs'; // Sesuaikan dengan nama tabel yang Anda gunakan

    protected $fillable = [
        'categories_id',
        'nama_produk',
        'id_member',
        'deskripsi_produk',
        'gambar_produk',
        'harga_modal',
        'harga_jual',
        'keuntungan',
        'file_produk',
        'status',
    ];
    protected $hidden = [];


    public function harga_jual()
    {
        $keuntungan = $this->getKeuntunganAttribute();

        $harga_jual = $this->harga_modal + $keuntungan;

        return $harga_jual;
    }
    public function getKeuntunganAttribute()
    {
        $persentasekeuntungan = 0.10;

        $keuntungan = $this->harga_modal * $persentasekeuntungan;

        return $keuntungan;
    }
    public function order()
    {
        return $this->hasOne(Order::class, 'id_barang', 'id');
    }
    public function member()
    {
        return $this->belongsTo(member::class, 'id_member', 'id');
    }
    public function category()
    {
        return $this->belongsTo(Kategori::class, 'categories_id', 'id');
    }
    public function Kategori()
    {
        return $this->belongsTo(kategori::class, 'categories_id', 'id');
    }
 
}
