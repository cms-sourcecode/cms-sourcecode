<?php

namespace App\Models;

use App\Http\Controllers\CategoryController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $guarded = [];
    protected $fillable = ['nama_kategori'];



    public function barangs()
    {
        return $this->hasMany(Barang::class, 'categories_id', 'id');
    }
}
