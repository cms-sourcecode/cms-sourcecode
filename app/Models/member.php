<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class member extends Model implements Authenticatable
{
    use HasFactory, AuthenticatableTrait;
    protected $table = 'members';
    protected $guarded = [];



    public function order()
    {
        return $this->hasMany(order::class);
    }
    public function ulasan()
    {
        return $this->hasMany(ulasan::class);
    }
    public function barang()
    {
        return $this->hasMany(Barang::class);
    }
    public function biodata()
    {
        return $this->hasOne(Biodata::class, 'id');
    }
}
