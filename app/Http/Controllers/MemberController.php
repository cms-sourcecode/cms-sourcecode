<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\member;
use App\Models\biodata;
use Illuminate\Support\Facades\Hash;

class memberController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function index()
    {
        $members = member::all();
        return view('admin.member.index', compact('members'));
    }

    public function create()
    {
        return view('admin.member.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat_detail' => 'required',
            'no_hp' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        member::create([
            'nama' => $request->nama,
            'username' => $request->username,
            'provinsi' => $request->provinsi,
            'kota'=> $request->kota,
            'kecamatan'=> $request->kecamatan,
            'alamat_detail' => $request->alamat_detail,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('member.index')->with('success', 'User berhasil ditambahkan.');
    }



    public function edit(member $member)
    {


        // Tampilkan halaman edit dengan data member
        return view('admin.member.edit', compact('member'));
    }

    public function update(Request $request, member $member)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat_detail' => 'required',
            'no_hp' => 'required',
            'password' => 'required',
            'email' => 'required|email|unique:members,email,' . $member->id,
        ]);

        $member->update(
            $validatedData
        );

        return redirect()->route('member.index')->with('success', 'User berhasil diperbarui.');
    }


    public function destroy(member $member)
    {
        $member->delete();

        return redirect()->route('member.index')->with('success', 'User berhasil dihapus.');
    }
}
