<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    // public function index()
    // {
    //     return view('admin.dashboard.index', [
    //         'title' => 'dashboard',
    //     ]);
    // }
    public function index()
    {
        // Periksa apakah pengguna atau admin yang sedang login
        if (auth()->check()) {
            // Pengguna atau admin yang sedang login
            if (auth()->guard('web')->check()) {
                // Ini adalah admin
                return view('admin.dashboard.index');
            } elseif (auth()->guard('member')->check()) {
                // Ini adalah pengguna/member
                return view('/');
            }
        } else {
            // Tidak ada yang login
            return redirect('/login');
        }
    }
}
