<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;

class CategoryController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function index()
    {
        $kategori = Kategori::all();
        return view('admin.kategori.index', compact('kategori'));
    }

    public function create()
    {
        return view('admin.kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required|string',

        ]);

        Kategori::create([
            'nama_kategori' => $request->nama_kategori,

        ]);

        return redirect()->route('kategori.index')->with('success', 'Kategori created successfully');
    }

    public function edit(Kategori $kategori)
    {
        return view('admin.kategori.edit', compact('kategori'));
    }

    public function update(Request $request, Kategori $kategori)
    {
        $request->validate([
            'nama_kategori' => 'required|string',

        ]);

        $kategori->update([
            'nama_kategori' => $request->nama_kategori,

        ]);

        return redirect()->route('kategori.index')->with('success', 'Kategori Updated successfully');
    }

    public function destroy(Kategori $kategori)
    {
        $kategori->delete();
        return redirect()->route('kategori.index')->with('success', 'Kategori deleted successfully');
    }

    public function dropdown()
    {
        $categories = kategori::all();
        return view('admin.kategori.index', ['categories' => $categories]);
    }
    
}
