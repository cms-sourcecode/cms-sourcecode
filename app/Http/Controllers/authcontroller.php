<?php

namespace App\Http\Controllers;

use App\Models\member;
use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class authcontroller extends Controller
{

    public function index()
    {
        return view('admin.auth.login');
    }

    public function login_member()
    {
        return view('user.auth.login_member');
    }

    public function login()
    {
        auth()->guard('web')->logout();
        auth()->guard('member')->logout();

        $credentials = request(["email", "password"]);

        if (auth()->guard('web')->attempt($credentials)) {
            $token = Auth::guard('api')->attempt($credentials);
            cookie()->queue(cookie('token', $token,));
            return redirect('/dashboard');
        }

        return back()->withErrors([
            'error' => 'email atau password salah'
        ]);
    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL()
        ]);
    }
    public function register(request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'username' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat_detail' => 'required',
            'no_hp' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:konfirmasi_password',
            'konfirmasi_password' => 'required|same:password',

        ]);

        if ($validator->fails()) {
            return response()->json(
                $validator->errors(),
                422
            );
        }

        $input = $request->all();

        $input['password'] = bcrypt($request->password);
        unset($input['konfirmasi_password']);
        $member = member::create($input);

        return response()->json([
            'data' => $member
        ]);
    }



    public function login_member_action(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('login_member')->withErrors($validator)->withInput();
        }

        $member = Member::where('email', $request->email)->first();

        if ($member) {
            if (Hash::check($request->password, $member->password)) {
                $request->session()->regenerate();
                auth()->guard('member')->login($member);
                // Jika Anda ingin mendapatkan data user yang berhasil login, gunakan $member, bukan auth()->user().
                // dd($member);
                return redirect()->route('home.index');
            } else {
                return redirect('login_member')->with('failed', 'Password Salah');
            }
        } else {
            return redirect('login_member')->with('failed', 'Email Tidak Ditemukan');
        }
    }

    public function register_member()
    {
        return view('user.auth.register_member');
    }

    public function register_member_action(request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'username' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat_detail' => 'required',
            'no_hp' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:konfirmasi_password',
            'konfirmasi_password' => 'required|same:password',

        ]);

        if ($validator->fails()) {
            session::flash('errors', $validator->errors()->toArray());
            return redirect('register_member');
        }

        $input = $request->all();

        $input['password'] = bcrypt($request->password);
        unset($input['konfirmasi_password']);
        $member = member::create($input);

        session::flash('success', 'Account succesfully created!');

        return redirect('login_member');
    }

    public function logout()
    {
        Auth::guard('web')->logout();

        return redirect('/login');
    }
    public function logout_member()
    {
        Auth::guard('member')->logout(); // Hapus semua data sesi
        return redirect('/');
    }
}
