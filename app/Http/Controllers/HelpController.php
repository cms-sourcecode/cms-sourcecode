<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function caraPemesanan()
    {
        return view('help.carapemesanan');
    }

    public function caraPenjualan()
    {
        return view('help.carapenjualan');
    }
}
