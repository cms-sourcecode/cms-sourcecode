<?php

namespace App\Http\Controllers;

use App\Models\biodata;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class BiodataController extends Controller
{
    public function index()
{
    $member = Auth::user();

    if (auth()->guard('member')->check()) {
        $member = auth()->guard('member')->user();
        $biodata = $member->biodata;
        return view('user.biodata.index', compact('biodata'));
    } else {
        // Pengguna tidak terotentikasi, lakukan sesuatu, seperti redirect ke halaman login
        return redirect()->route('login');
    }
}

    public function showProfile()
    {
        $user = Auth::user();
        $biodata = Biodata::find(); // Gantilah dengan cara mendapatkan biodata sesuai dengan kebutuhan aplikasi Anda

        return view('user.profile', compact('biodata'));
    }

    public function update(Request $request, biodata $biodata)
    {
        if ($request->has('password')) {
            $request->merge(['password' => Hash::make($request->input('password'))]);
        }

        $biodata->update($request->all());
        return redirect()->route('biodata.index')->with('success', 'Data berhasil diperbarui!');
    }
}
