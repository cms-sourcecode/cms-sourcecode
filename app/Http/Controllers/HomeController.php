<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Kategori;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    // public function index()
    // {
    //     $barangs = Barang::paginate(8);

    //     return view('user.home.index', compact('barangs'));
    // }
    public function index()
    {
        $barangs = Barang::where('status', 'aktif')->paginate(4);
        return view('user.home.index', compact('barangs'));
    }

    public function produkByKategori($categories_id)
    {
        // Cari kategori berdasarkan id
        $kategori = Kategori::with('barang')->find($categories_id);

        // Jika kategori tidak ditemukan, tampilkan pesan kesalahan
        if (!$kategori) {
            return abort(404, "No Category Found With ID: $categories_id");
        }

        // Ambil produk berdasarkan kategori
        $produk = $kategori->barang;

        // Ambil semua data kategori dan barang (tidak perlu disimpan ke variabel)
        $categories = Kategori::all();

        // Render view dan kirim data ke view
        return view('user.home.index', compact('produk', 'categories'));
    }
}
