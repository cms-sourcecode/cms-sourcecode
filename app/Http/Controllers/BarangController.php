<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\kategori;
use App\Models\member;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;

class BarangController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function index()
    {
        // $categories = kategori::all();
        // $products = Barang::all();

        // return view('home.index', compact('categories', 'barangs'));
        //     // Query pertama: Mengambil data yang diperlukan dari tabel 'barang'
        // $barangs = Barang::select('id', 'categories_id', 'nama_produk','id_member', 'deskripsi_produk','gambar_produk','harga_modal', 'harga_jual', 'keuntungan','created_at')->get();

        // // Query kedua: Mengambil data terkait dari tabel 'kategori' dan 'member'
        // $barangs = $barangs->load(['kategori', 'member']);

        // return view('barang.index', compact('barangs'));

        // {
        //     $barangs = Barang::where('status', 'aktif')->get();
        //     return view('admin.barang.index', compact('barangs'));
        // }

        // // // Filter out barangs without kategori
        // // $barangs = $barangs->filter(function ($barang) {
        // //     return $barang->kategori !== null;
        // // });

        $barangs = Barang::with(['kategori', 'member'])->get();
        return view('admin.barang.index', compact('barangs'));

        // return view('barang.index', [
        //     'barangs' => $barangs
        // ]);
    }

    public function create()
    {
        $categories = kategori::all();
        $members = member::all();
        return view('admin.barang.create', compact('categories', 'members'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'categories_id' => 'required|exists:categories,id',
            'nama_produk' => 'required|string',
            'id_member' => 'required',
            'deskripsi_produk' => 'required|string',
            'gambar_produk' => 'required|image|mimes:jpeg,png,jpg,webp',
            'harga_modal' => 'required|numeric',
            // 'harga_jual' => 'required|numeric',

            'file_produk' => 'required|mimes:zip,rar',
            // 'status' => 'required',
        ]);

        $gambar_produk = $request->file('gambar_produk');
        $nama_gambar = time() . '.' . $gambar_produk->getClientOriginalExtension();
        $gambar_produk->move(public_path('uploads'), $nama_gambar);

        $file_produk = $request->file('file_produk');
        $nama_file = $file_produk->getClientOriginalName();
        $file_produk->move(public_path('files'), $nama_file);

        $harga_modal = $request->input('harga_modal');

        $barang = new barang(['harga_modal' => $harga_modal]);

        $harga_jual = $barang->harga_jual();
        $keuntungan = $barang->getKeuntunganAttribute();
        // $harga_jual = $request->input('harga_jual');
        // $keuntungan = ($harga_jual - $harga_modal);

        Barang::create([
            'categories_id' => $request->categories_id,
            'nama_produk' => $request->nama_produk,
            'id_member' => $request->id_member,
            'deskripsi_produk' => $request->deskripsi_produk,
            'gambar_produk' => $nama_gambar,
            'harga_modal' => $harga_modal,
            'harga_jual' => $harga_jual,
            'keuntungan' => $keuntungan,
            'file_produk' => $nama_file,
            // 'tgl_publish' => $request->tgl_publish,
            // 'status' => $request->status,
        ]);

        return redirect()->route('barang.index')->with('success', 'Produk berhasil ditambahkan.');
    }

    public function edit(Barang $barang)
    {
        $categories = kategori::all();
        $members = member::all();
        return view('admin.barang.edit', compact('barang', 'categories', 'members'));
    }

    public function update(Request $request, Barang $barang)
    {
        $request->validate([
            'categories_id' => 'required|exists:categories,id',
            'nama_produk' => 'required|string',
            'id_member' => 'required',
            'deskripsi_produk' => 'required|string',

            'harga_modal' => 'required|numeric',
            // 'harga_jual' => 'required|numeric',

            // 'status' => 'required',
        ]);

        $input = $request->all();

        if ($request->has('gambar_produk')) {
            File::delete('uploads/' . $barang->gambar_produk);
            $gambar_produk = $request->file('gambar_produk');
            $nama_gambar = time() . rand(1, 9) . '.' . $gambar_produk->getClientOriginalExtension();
            $gambar_produk->move('uploads', $nama_gambar);
            $input['gambar_produk'] = $nama_gambar;
        } else {
            unset($input['gambar_produk']);
        }

        if ($request->has('file_produk')) {
            File::delete('files/' . $barang->file_produk);
            $file_produk = $request->file('file_produk');
            $nama_file = $file_produk->getClientOriginalName();
            $file_produk->move('files', $nama_file);
            $input['file_produk'] = $nama_file;
        } else {
            unset($input['file_produk']);
        }

        $barang->harga_modal = $request->harga_modal;
        $barang->harga_jual = $barang->harga_jual();
        $barang->keuntungan = $barang->getKeuntunganAttribute();

        $barang->update($input);

        return redirect()->route('barang.index')->with('success', 'Produk berhasil diperbarui.');
    }

    public function destroy(Barang $barang)
    {

        file::delete('uploads/' . $barang->gambar_produk);
        $barang->delete();
        file::delete('files/' . $barang->file_produk);
        $barang->delete();

        return redirect()->route('barang.index')->with('success', 'Produk berhasil dihapus.');
    }

    // ProductController.php

    // app/Http/Controllers/BarangController.php

    public function showDetail($namaProduk)
    {
        // Cari barang berdasarkan nama_produk
        $barangs = Barang::where('nama_produk', $namaProduk)->with('order')->get();

        if (!$barangs) {
            return redirect()->route('home.index')->with('error', 'Produk tidak ditemukan.');
        }

        return view('user.home.detail', compact('barangs'));
    }

    // BarangController.php



    public function search(Request $request)
    {
        $query = $request->input('query');
        $barangsquery = Barang::where('status', 'aktif');

        if ($query) {
            $barangsquery->where(function ($queryBuilder) use ($query) {
                $queryBuilder->where('nama_produk', 'like', '%' . $query . '%')
                    ->orWhere('deskripsi_produk', 'like', '%' . $query . '%');
            });
        }
        $results = $barangsquery->paginate(4);

        $categories = Kategori::all();

        return view('user.home.search', compact('results', 'categories'));
    }




    public function produkByKategori($kategori)
    {
        try {
            // Cari kategori berdasarkan id
            $kategori = Kategori::with('barangs')->findOrFail($kategori);
            $categories = Kategori::all();
            // Ambil produk berdasarkan kategori dan paginasi
            $barangs = $kategori->barangs()->where('status', 'aktif')->paginate(4); // Sesuaikan jumlah item per halaman

            // Render view dan kirim data ke view
            return view('user.home.index', compact('barangs', 'categories'));
        } catch (ModelNotFoundException $e) {
            // Jika kategori tidak ditemukan, tampilkan pesan kesalahan
            return abort(404, "Tidak ada Kategori Ditemukan dengan ID: $kategori");
        }
    }

    // public function getBarangByCategory($categoryId)
    // {
    //     $barangs = Barang::where('categories_id', $categoryId)->get();
    //     return response()->json($barangs);
    // }

    // public function dropdown(Request $request)
    // {
    //     $categories = kategori::all();
    //     $query = Barang::query();

    //     if ($request->filled('kategori')) {
    //         $query->where('categories_id', $request->input('categories'));
    //     }

    //     $barangs = $query->get();

    //     return view('home.index', compact('categories', 'barangs'));
    // }

    public function updateStatus(Request $request, Barang $barang)
    {
        $request->validate([
            'status' => 'required|in:aktif,tidak aktif',
        ]);

        $barang->update([
            'status' => $request->input('status'),
        ]);

        return redirect()->route('barang.index')->with('success', 'Status produk berhasil diperbarui.');
    }
    // BarangController.php

    public function showByNamaProduk($namaProduk)
    {
        // Cari barang berdasarkan nama_produk
        $barang = Barang::where('nama_produk', $namaProduk)->first();

        if (!$barang) {
            return redirect()->route('home.index')->with('error', 'Produk tidak ditemukan.');
        }

        return view('user.home.detail', compact('barang'));
    }

    // app/Http/Controllers/BarangController.php

    public function showByKategoriAndNamaProduk($kategoriId, $namaProduk)
    {
        // Cari barang berdasarkan kategori dan nama_produk
        $barang = Barang::where('categories_id', $kategoriId)
            ->where('nama_produk', $namaProduk)
            ->with('category')
            ->first();

        if (!$barang) {
            return redirect()->route('home.index')->with('error', 'Produk tidak ditemukan.');
        }

        return view('user.home.detail', compact('barang'));
    }
}
