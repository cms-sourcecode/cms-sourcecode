<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\order;
use App\Models\orderdetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    // public function __construct()
    // {
    //     $this->middleware('auth:api', ['except' => 'index']);
    //     $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    // }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orders = order::with(['member', 'barang'])->get();
        // return view('pesanan.index', compact('orders'));

        return response()->json([
            'data' => $orders
        ]);
    }

    public function list()
    {
        $responseFromIndex = $this->index();

        $dataFromIndex = json_decode($responseFromIndex->getContent(), true)['data'];

        return view('admin.pesanan.index', [
            'title' => 'Pesanan',
            'orders' => $dataFromIndex,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_member' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                $validator->errors(),
                422
            );
        }

        $input = $request->all();
        $order = order::create($input);

        for ($i = 0; $i < count($input['id_barang']); $i++) {
            orderdetail::create([
                'id_order' => $order['id'][$i],
                'id_barang' => $input['id_barang'][$i],
                'total' => $input['total'][$i],
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(order $order)
    {
        return response()->json([
            'data' => $order
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, order $order)
    {
        $validator = Validator::make($request->all(), [
            'id_member' => 'required',
            'buktiPembayaran' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        if ($validator->fails()) {
            return response()->json(
                $validator->errors(),
                422
            );
        }

        $input = $request->all();
        $order->update($input);

        if ($request->hasFile('buktiPembayaran')) {
            $request->validate([
                'buktiPembayaran' => 'required|image|mimes:jpeg,png,jpg',
            ]);

            // Hapus bukti pembayaran lama jika ada
            if ($order->bukti_pembayaran) {
                Storage::disk('bukti/')->delete($order->bukti_pembayaran);
            }

            // Simpan bukti pembayaran baru ke server
            $imagePath = $request->file('buktiPembayaran')->store('bukti_pembayaran', 'bukti');

            // Update kolom bukti_pembayaran pada pesanan
            $order->update(['bukti_pembayaran' => $imagePath]);
        }
        // orderdetail::where('id_order', $order['id'])->delete();

        // for ($i = 0; $i < count($input['id_produk']); $i++) {
        //     orderdetail::create([
        //         'id_order' => $order['id'][$i],
        //         'id_produk' => $input['id_produk'][$i],
        //         'total' => $input['total'][$i],
        //     ]);
        // }

        return response()->json([
            'message' => 'succes',
            'data' => $order
        ]);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(order $order)
    {
        $order->delete();

        return response()->json([
            'message' => 'succes'
        ]);
    }
    public function filter(request $request)
    {
        // Mendapatkan tanggal mulai dari request
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        // Validasi tanggal mulai
        $request->validate([
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);

        // Mendapatkan pesanan berdasarkan rentang tanggal
        $orders = order::where(function ($query) use ($start_date, $end_date) {
            $query->whereDate('created_at', '>=', $start_date)
                ->whereDate('created_at', '<=', $end_date);
        })
            ->get();

        // Menampilkan view 'pesanan.index' dengan data pesanan dan tanggal mulai
        return view('admin.pesanan.index', compact('orders', 'start_date', 'end_date'));
    }
    public function cetak(request $request)
    {
        $start_date = Carbon::parse($request->input('start_date'))->startOfDay();
        $end_date = Carbon::parse($request->input('end_date'))->endOfDay();

        $orders = Order::where('created_at', '>=', $start_date)
            ->where('created_at', '<=', $end_date)
            ->get();

        return view('admin.pesanan.cetak', compact('orders', 'start_date', 'end_date'));
    }

    // public function uploadBukti(Request $request, $id)
    // {
    //     $request->validate([
    //         'buktiPembayaran' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //     ]);

    //     $barangs = Barang::findOrFail($id);

    //     $imageName = time() . '.' . $request->buktiPembayaran->extension();
    //     $request->buktiPembayaran->move(public_path('uploads'), $imageName);

    //     // Simpan data ke dalam tabel recappesanan
    //     RecapPesanan::create([
    //         'id_barang' => $barangs->id,
    //         'bukti_pembayaran' => $imageName,
    //         // Tambahkan kolom lain sesuai kebutuhan
    //     ]);

    //     return redirect()->back()->with('success', 'Bukti pembayaran berhasil diupload.');
    // }

    public function uploadBuktiPembayaran(Request $request, $id)
    {
        $request->validate([
            'buktiPembayaran' => 'required|image|mimes:jpeg,png,jpg',
        ]);
        $buktiPembayaran = $request->file('buktiPembayaran');
        $nama_bukti = time() . '.' . $buktiPembayaran->getClientOriginalExtension();
        $buktiPembayaran->move(public_path('bukti'), $nama_bukti);

        $member = Auth::guard('member')->user();
        $barang = Barang::find($id);

        // Periksa status bukti pembayaran sebelum melanjutkan
        $existingOrder = Order::where('id_member', $member->id)
            ->where('id_barang', $barang->id)
            ->where('bukti_pembayaran_status', 'uploaded')
            ->first();

        if ($existingOrder) {
            return redirect()->back()->with('error', 'Anda sudah mengunggah bukti pembayaran sebelumnya.');
        }

        Order::create([
            'id_member' => $member->id,
            'id_barang' => $barang->id,
            'bukti_pembayaran' => $nama_bukti,
            'bukti_pembayaran_status' => 'uploaded',
            // tambahkan kolom lain sesuai kebutuhan
        ]);

        return redirect()->route('barang.detail', ['id' => $barang->id])->with('success', 'Bukti pembayaran berhasil diupload.');
    }

    public function downloadfile($id)
    {
        $barang = Barang::find($id);

        $filePath = public_path("files/{$barang->file_produk}");

        return response()->download($filePath);
    }

    public function konfirmasiPembayaran($id)
    {
        $order = Order::find($id);

        // Pastikan bahwa pesanan ditemukan
        if (!$order) {
            return redirect()->back()->with('error', 'Pesanan tidak ditemukan.');
        }

        // Update status pesanan menjadi "Dikonfirmasi"
        $order->update(['bukti_pembayaran_status' => 'confirmed']);

        return redirect()->back()->with('success', 'Pembayaran berhasil dikonfirmasi.');
    }

    public function batalkanPembayaran($id)
    {
        // Ambil pesanan berdasarkan ID
        $order = order::findOrFail($id);

        // Lakukan tindakan pembatalan pembayaran
        // Contoh: Update status pembatalan dan kembalikan stok barang jika perlu
        
        // Hapus pesanan dari database
        $order->delete();

        // Redirect atau kembalikan response sesuai kebutuhan
        return redirect()->back()->with('success', 'Pembayaran berhasil dibatalkan dan pesanan dihapus.');
    }

    public function editImage(Request $request, $id)
    {
        $barang = barang::findOrFail($id);

        // Validasi gambar yang diedit
        $request->validate([
            'buktiPembayaran' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        // Tangani gambar yang diedit
        File::delete('bukti/' . $barang->order->bukti_pembayaran);
        $buktiPembayaran = $request->file('buktiPembayaran');
        $nama_bukti = time() . '.' . $buktiPembayaran->getClientOriginalExtension();
        $buktiPembayaran->move(public_path('bukti'), $nama_bukti);

        // Perbarui kolom bukti_pembayaran order dengan nama gambar baru
        $barang->order->update(['bukti_pembayaran' => $nama_bukti]);

        return redirect()->back()->with('success', 'Gambar berhasil diperbarui.');
    }
}
