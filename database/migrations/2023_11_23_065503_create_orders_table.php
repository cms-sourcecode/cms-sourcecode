<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('id_member');
            $table->integer('id_barang');
            $table->integer('total');
            $table->string('action');
            $table->timestamps();
        });
       
        Schema::create('OrderDetails', function (Blueprint $table) {
            $table->id();
            $table->integer('id_member');
            $table->integer('id_barang');
            $table->string('bukti_pembayaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
