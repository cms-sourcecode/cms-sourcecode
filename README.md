# 👩‍💻CMS-SourceCode(MarketPlace)

"Proyek ini bertujuan untuk membuat platform marketplace yang memungkinkan penjual dan pembeli berinteraksi dan melakukan transaksi dengan aman,untuk menghasilkan program atau aplikasi yang dapat dijalankan oleh komputer atau sistem lainnya,serta untuk mempermudah seseorang dalam membuat web ketika terkendala dalam pekerjaanya". Platform ini dilengkapi dengan fitur-fitur berikut:

-   Registrasi dan otentikasi pengguna
-   Menampilkan produk dengan gambar, deskripsi, dan harga
-   Penilaian dan ulasan produk
-   Pilihan Produk dan proses checkout
-   Riwayat transaksi pengguna
-   Notifikasi transaksi

## Anggota 🚀Kelompok CMS-SourceCode

-   🚀 Anugrah Jizdan Medika
-   🚀 Indah Sari Dewi Parapat
-   🚀 Adinda Salsabila Ramadhani

## 🛠 Tech

Aplikasi ini dibuat dengan menggunakan:

\*_Teknologi dan Alat_

-   [XAMPP](https://www.apachefriends.org/download.html) Digunakan sebagai server lokal untuk pengembangan dan pengujian. XAMPP menyediakan lingkungan pengembangan PHP dan MySQL.
-   [Visual Studio Code](https://code.visualstudio.com/) Digunakan sebagai editor kode untuk pengembangan proyek. VS Code merupakan editor sumber terbuka yang ringan dan powerful.
-   [Google Chrome](https://www.google.co.id/chrome/?brand=YTUH&gclid=Cj0KCQiA4Y-sBhC6ARIsAGXF1g6Qs57krcxHH00hNVn5UhTufUlKeQE7tqWOEFD45R5LLuDRahdJP4AaApCoEALw_wcB&gclsrc=aw.ds) Browser web yang digunakan untuk menguji dan menjalankan aplikasi. Proyek ini dioptimalkan untuk kompatibilitas dengan Google Chrome.
-   [W3School](https://www.w3schools.com/) Sumber daya pembelajaran web yang digunakan sebagai referensi dan panduan selama pengembangan.

\*_Bahasa Pemrograman dan Framework_

-   [HTML](https://www.w3schools.com/html/default.asp) Bahasa markup digunakan untuk struktur halaman web.
-   [CSS](https://www.w3schools.com/css/default.asp) Bahasa gaya digunakan untuk mendesain dan mempercantik tata letak halaman web.
-   [PHP](http://localhost/phpmyadmin/) Bahasa pemrograman sisi server digunakan untuk logika bisnis dan berinteraksi dengan database.
-   [Bootstrap](https://www.w3schools.com/bootstrap/bootstrap_ver.asp) Framework CSS yang digunakan untuk merancang tata letak responsif dan menarik.
-   [Modal](https://www.w3schools.com/bootstrap5/bootstrap_modal.php) Komponen Bootstrap yang digunakan untuk menampilkan jendela modal interaktif.
-   [SweetAlert](https://sweetalert2.com/download#download) Library JavaScript yang digunakan untuk membuat pesan peringatan yang menarik dan interaktif.

\*_Alat Pengembangan dan Kolaborasi_

-   [GitLab](https://gitlab.com/cms-sourcecode/cms-sourcecode.git) Platform manajemen repositori Git yang digunakan untuk menyimpan dan berkolaborasi pada kode sumber proyek.
-   [Git Bash](https://git-scm.com/downloads) Terminal Git yang digunakan untuk berinteraksi dengan Git dan menjalankan perintah Git pada lingkungan Windows.

## ⚡️Instalasi

### Berikut adalah langkah-langkah untuk menginstal proyek ini di lingkungan lokal Anda:

1. Dapatkan URL Repositori Git:

-   Kunjungi proyek Laravel yang ingin Anda clone di GitLab.
-   Salin URL repositori Git yang terlihat seperti
    `https://gitlab.com/cms-sourcecode/cms-sourcecode.git`

2. Buka Terminal:

-   Buka terminal atau command prompt di komputer Anda.

3. Pindah ke Direktori Tujuan:

-   Pindah ke direktori tempat Anda ingin menyimpan proyek Laravel yang akan di-clone. Gunakan perintah `cd nama-direktori` untuk pindah ke direktori yang sesuai.

4. Clone Repositori:

-   Gunakan perintah git clone diikuti oleh URL repositori Git yang telah Anda salin sebelumnya. Contoh:
-   git clone

```bash
 https://gitlab.com/cms-sourcecode/cms-sourcecode.git
```

5. Masukkan Kredensial GitLab:

-   Jika repositori bersifat privat dan memerlukan autentikasi, Git akan meminta Anda untuk memasukkan username dan password atau token akses. Gunakan token akses jika Anda menggunakan otentikasi dua faktor atau jika Anda ingin menggunakan metode otentikasi yang lebih aman.

6. Pindah ke Direktori Proyek:

-   Setelah proses cloning selesai, pindah ke direktori proyek dengan perintah:
    _bash copy_

```bash
cd nama-proyek
```

7. Instal Dependensi:

-   Jika proyek Laravel menggunakan Composer (yang umumnya digunakan), jalankan perintah berikut untuk menginstal dependensi:

```bash
composer install
```

8. Konfigurasi File .env:

-   Duplikat file `.env.example` menjadi `.env` dan atur koneksi database dan pengaturan lainnya sesuai kebutuhan proyek Anda.

9. Generate Key Aplikasi:

-   Jalankan perintah untuk menghasilkan kunci aplikasi Laravel:

```bash
php artisan key:generate
```

10. Sekarang, proyek Laravel telah berhasil di-clone ke mesin lokal Anda. Anda dapat menjalankan proyek tersebut dengan menggunakan server Laravel bawaan:

```bash
php artisan serve
```

-   Buka browser dan akses `http://localhost:8000`

## 🧑‍🏫👩‍🏫 Requirement

\*_Ketentuan:_
Aplikasikan materi-materi yang telah dipelajari sebelumnya di antaranya:

-   Membuat ERD
-   Migrations
-   Model + Eloquent
-   Controller
-   Laravel Auth + Middleware
-   View(Blade)
-   CRUD (Create Read Update Delete)
-   Eloquent Relationships
-   Laravel + Library/Packages
-   Catatan: Kreasikan sesuai kelompok masing-masing.

\*_Spesifikasi:_

-   Aplikasi dibuat menggunakan Laravel 8 atau versi 8 keatas.
-   Tampilan simple menggunakan bootstrap CSS atau template yang sesuai.
-   Tugas dikerjakan berkelompok (3 orang / 4 Orang) berkolaborasi
-   menggunakan Gitlab/Github/Bitbucket.
-   Tugas dideploy di hostingan atau di heroku
-   Tampilkan gambar ERD dan link deploy di Readme.md

## Dokumentasi Lebih Lanjut

Lihat [dokumentasi lengkap](https://gitlab.com/cms-sourcecode/cms-sourcecode.git) untuk informasi lebih lanjut.

## 🔗 Links

[📑LAPORAN TRD KELOMPOK-SOURCECODE](https://docs.google.com/document/d/1ndmaMgLYDy1ckjxss1apTxhHMiLqsap_/edit?usp=sharing&ouid=107810606191132262185&rtpof=true&sd=true)📑

[📽VIDIO PRESENTASI DAN TAMPILAN WEB KELOMPOK-SOURCECODE](https://drive.google.com/drive/folders/1FHyRhn2XeaARGngSRqS1r8WZ4-pTNlPw)📽

## 📝 Lisensi

Proyek ini dilisensikan di bawah [GarudaCyberIndonesia](https://kelas.garudacyberinstitute.id/).

## Kontak dan Dukungan

Jika Anda memiliki pertanyaan, silakan hubungi kami di [adinda1911mail@gmail.com].

## Demo

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](http://127.0.0.1:8000/)

## 🎞️ Screenshots

![App Screenshot](./public/images/Sourcode1.png)
