<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.includes.head')
</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
    @include('admin.includes.navbar')
            

    @include('admin.includes.sidebar')

                        <!-- Main Content -->
    @include('admin.includes.main')

    @include('admin.includes.footer')
            
        </div>

        @include('admin.includes.js')
</body>

</html>