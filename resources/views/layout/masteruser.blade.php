<!--  -->
<!DOCTYPE html>
<html lang="en">

<head>

  @include('user.includes.head')

</head>

<body class="layout-3">
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>

    @include('user.includes.navbar')

      <!-- Main Content -->
    @include('user.includes.main')

      <!--  -->
    </div>
    <!-- FOOTER -->
    @include('user.includes.footer')


  </div>



  <!-- dropdown kategori -->
  <!-- General JS Scripts -->
  @include('user.includes.js')


</body>

</html>