@include('user.includes.head')
<body class="layout-3">
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            @include('user.includes.navbar')
            <div class="site-content py-5 mt-6">
                <style>    

.slider-container {
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 100%;
  margin-top: 60px;
  margin-bottom: 60px;
}

.left-slide {
  height: 100%;
  width: 35%;
  position: absolute;
  top: 0;
  left: 0;
  transition: transform 0.5s ease-in-out;
}

.left-slide > div {
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: #fff;
}

.left-slide h1 {
  font-size: 26px;
  margin-bottom: 10px;
  margin-top: -30px;
}

.right-slide {
  height: 100%;
  position: absolute;
  top: 0;
  left: 35%;
  width: 60%;
  transition: transform 0.5s ease-in-out;
}

.right-slide > div {
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  height: 100%;
  width: 100%;
}

button {
  background-color: #fff;
  border: none;
  color: #aaa;
  cursor: pointer;
  font-size: 16px;
  padding: 15px;
}

button:hover {
  color: #222;
}

button:focus {
  outline: none;
}

.slider-container .action-buttons button {
  position: absolute;
  left: 35%;
  top: 60%;
  z-index: 100;
}

.slider-container .action-buttons .down-button {
  transform: translateX(-100%);
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.slider-container .action-buttons .up-button {
  transform: translateY(-100%);
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

h2.text {
text-align: center; /* Center-align the title */
}

/* Styles for each step in the ordering process */

@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;400&display=swap');

:root {
  --primary-color: #ffffff;
  --secondary-color: #272c60;
}

* {
  box-sizing: border-box;
}

body {
  background-color: var(--primary-color);
  font-family: 'Poppins', sans-serif;
  margin: 0;
}

header {
  padding: 1rem;
  display: flex;
  justify-content: flex-end;
  background-color: var(--secondary-color);
}

.search {
  background-color: transparent;
  border: 2px solid var(--primary-color);
  border-radius: 50px;
  font-family: inherit;
  font-size: 1rem;
  padding: 0.5rem 1rem;
  color: #fff;
}

.search::placeholder {
  color: #585c97;
}

.search:focus {
  outline: none;
  background-color: var(--primary-color);
}

main {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.movie {
  width: 300px;
  margin: 1rem;
  background-color: var(--secondary-color);
  box-shadow: 0 4px 5px rgba(0, 0, 0, 0.2);
  position: relative;
  overflow: hidden;
  border-radius: 3px;
}

.movie img {
  width: 100%;
  height: 60%;
}

.movie-info {
  color: #eee;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap:0.2rem;
  padding: 0.5rem 1rem 1rem;
  letter-spacing: 0.5px;
}

.movie-info h3 {
  margin-top: 0;
}

.movie-info span {
  background-color: var(--primary-color);
  padding: 0.25rem 0.5rem;
  border-radius: 3px;
  font-weight: bold;
}

.movie-info span.green {
  color: lightgreen;
}

.movie-info span.orange {
  color: orange;
}

.movie-info span.red {
  color: red;
}

.overview {
  background-color: #fff;
  padding: 2rem;
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  max-height: 100%;
  transform: translateY(101%);
  overflow-y: auto;
  transition: transform 0.3s ease-in;
  font-style: italic;
}

.movie:hover .overview {
  transform: translateY(0);
}



</style>
<div class= "container">
<br><br><h2 class="text aligh-center mt-4" >Cara Pemesanan </h2>
<div class=" slider-container mt-5">
      <div class="left-slide">
        <div style="background-color: #886da4 ">
          <h1>1. Pendaftaran Akun Pembeli</h1>
          <p></p>
        </div>
        <div style="background-color: #2A86BA">
          <h1>2. Pencarian Source Code</h1>
          <p></p>
        </div>
        {{-- <div style="background-color: #FFB866">
          <h1>3. Penawaran dan Negosiasi</h1>
          <p></p>
        </div> --}}
        <div style="background-color: #008080 ">
          <h1>3. Pembayaran</h1>
          <p></p>
        </div>
        <div style="background-color: #4775d1">
          <h1>4. Konfirmasi Pembayaran</h1>
          <p></p>
        </div>
        <div style="background-color: #b366ff">
          <h1>5. Download Source Code</h1>
          <p></p>
        </div>
        
      </div>
      <div class="right-slide">
      <div style="background-image: url('{{ asset('images/gambar28.jpeg') }}');"></div>
    <div style="background-image: url('{{ asset('images/gambar3.jpeg') }}');"></div>
     {{-- <div style="background-image: url('{{ asset('images/gambar2.jpeg') }}');"></div> --}}
     <div style="background-image: url('{{ asset('images/gambar37.JPEG') }}');"></div>
    <div style="background-image: url('{{ asset('images/gambar18.jpeg') }}');"></div>
    <div style="background-image: url('{{ asset('images/gambar17.jpeg') }}');"></div>
      </div>
      <div class="action-buttons">
        <button class="down-button">
          <i class="fas fa-arrow-down"></i>
        </button>
        <button class="up-button">
          <i class="fas fa-arrow-up"></i>
        </button>
      </div>
    </div>

    
    
    
    </div>
    <h2>&nbsp &nbsp OverView</h2>
    <main id="main">
                        <!-- Film 1 -->
                        <div class="movie">
                            <img src="/images/gambar17.JPEG"
                                alt="Pendaftaran Akun Pembeli ">
                            <div class="movie-info">
                                <h3>Pendaftaran Akun Pembeli</h3>
                                <span class="orange">1.</span>
                            </div>
                            <div class="overview">
                                <h3>Description</h3>
                                Pembeli memulai dengan membuat akun di platform web.
                                Pembeli mengisi informasi profil.
                            </div>
                        </div>

                        <!-- Film 2 -->
                        <div class="movie">
                            <img src="/images/gambar18.jpeg"
                                alt="Pencarian Source Code">
                            <div class="movie-info">
                                <h3>Pencarian Source Code</h3>
                                <span class="orange">2.</span>
                            </div>
                            <div class="overview">
                                <h3>Description</h3>
                                Pembeli mencari source code yang sesuai dengan kebutuhan mereka.
                                Fitur pencarian platform memungkinkan pembeli untuk menyaring dan menemukan source code yang tepat.
                            </div>
                        </div>

                        {{-- <!-- Film 3 -->
                        <div class="movie">
                            <img src="/images/gambar37.jpeg"
                                alt="Penawaran dan Negosiasi (Opsional)">
                            <div class="movie-info">
                                <h3>Penawaran dan Negosiasi</h3>
                                <span class="orange">3.</span>
                            </div>
                            <div class="overview">
                                <h3>Description</h3>
                                Jika diizinkan oleh penjual, pembeli dapat memberikan penawaran atau bernegosiasi harga source code.
                                Komunikasi antara pembeli dan penjual memungkinkan adanya interaksi dan kesepakatan yang lebih baik.
                            </div>
                        </div> --}}

                        <!-- Film 4 -->
                        <div class="movie">
                            <img src="/images/gambar2.jpeg"
                                alt="Pembayaran">
                            <div class="movie-info">
                                <h3>Pembayaran</h3>
                                <span class="orange">3.</span>
                            </div>
                            <div class="overview">
                                <h3>Description</h3>
                                Pembeli melakukan pembayaran melalui sistem pembayaran yang disediakan oleh platform.
                                Pembeli mengikuti proses pembayaran yang aman dan dapat dipercaya.
                            </div>
                        </div>

                        <!-- Film 5-->
                        <div class="movie">
                            <img src="/images/gambar3.jpeg"
                                alt="Konfirmasi Pembayaran">
                            <div class="movie-info">
                                <h3>Konfirmasi Pembayaran</h3>
                                <span class="orange">4.</span>
                            </div>
                            <div class="overview">
                                <h3>Description</h3>
                                Setelah pembayaran berhasil, platform memberikan konfirmasi kepada pembeli.
                                Konfirmasi melibatkan rincian pembayaran dan informasi tentang proses selanjutnya.
                                
                            </div>
                        </div>

                        <!-- Film 6 -->
                        <div class="movie">
                            <img src="/images/gambar28.jpeg"
                                alt="Download Source Code">
                            <div class="movie-info">
                                <h3>Download Source Code</h3>
                                <span class="orange">5.</span>
                            </div>
                            <div class="overview">
                                <h3>Description</h3>
                                pembeli diberikan akses untuk mendowload SourceCode
                                Pembeli dapat mengunduh source code atau mendapatkan akses langsung ke repositori version control.
                                
                            </div>
                        </div>

                       <!-- Tambahkan div .movie untuk film-film berikutnya -->
                    </main>
    </div>
</div>
            
    @include('user.includes.footer')
    @include('user.includes.js')
    <script>

      const sliderContainer = document.querySelector('.slider-container');
      const slideRight = document.querySelector('.right-slide');
      const slideLeft = document.querySelector('.left-slide');
      const upButton = document.querySelector('.up-button');
      const downButton = document.querySelector('.down-button');
      const slidesLength = slideRight.querySelectorAll('div').length;

      let activeSlideIndex = 0;

      slideLeft.style.top = `-${(slidesLength - 1) * 100}vh`;

      upButton.addEventListener('click', () => changeSlide('up'));
      downButton.addEventListener('click', () => changeSlide('down'));

      const changeSlide = (direction) => {
        const sliderHeight = sliderContainer.clientHeight;
        if (direction === 'up') {
          activeSlideIndex++;
          if (activeSlideIndex > slidesLength - 1) {
            activeSlideIndex = 0;
          }
        } else if (direction === 'down') {
          activeSlideIndex--;
          if (activeSlideIndex < 0) {
            activeSlideIndex = slidesLength - 1;
          }
        }

        slideRight.style.transform = `translateY(-${activeSlideIndex * sliderHeight}px)`;
        slideLeft.style.transform = `translateY(${activeSlideIndex * sliderHeight}px)`;
      };

      const API_URL = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c&page=1';
                        const IMG_PATH = 'https://image.tmdb.org/t/p/w1280';
                        const SEARCH_API = 'https://api.themoviedb.org/3/search/movie?api_key=3fd2be6f0c70a2a598f084ddfb75487c&query="';

                        const main = document.getElementById('main');
                        const form = document.getElementById('form');
                        const search = document.getElementById('search');

                        // Get initial movies
                        getMovies(API_URL);

                        async function getMovies(url) {
                            const res = await fetch(url);
                            const data = await res.json();

                            // Tampilkan hanya 8 film pertama
                            showMovies(data.results.slice(0, 8));
                        }

                        function showMovies(main) {
                            main.innerHTML = '';

                            movies.forEach((movie) => {
                                const { title, poster_path, vote_average, overview } = movie;

                                const movieEl = document.createElement('div');
                                movieEl.classList.add('movie');

                                movieEl.innerHTML = `
                                    <img src="${IMG_PATH + poster_path}" alt="${title}">
                                    <div class="movie-info">
                                        <h3>${title}</h3>
                                        <span class="${getClassByRate(vote_average)}">${vote_average}</span>
                                    </div>
                                    <div class="overview">
                                        <h3>Overview</h3>
                                        ${overview}
                                    </div>
                                `;
                                main.appendChild(movieEl);
                            });
                        }

                        function getClassByRate(vote) {
                            if (vote >= 8) {
                                return 'green';
                            } else if (vote >= 5) {
                                return 'orange';
                            } else {
                                return 'red';
                            }
                        }

                        form.addEventListener('submit', (e) => {
                            e.preventDefault();

                            const searchTerm = search.value;

                            if (searchTerm && searchTerm !== '') {
                                getMovies(SEARCH_API + searchTerm);

                                search.value = '';
                            } else {
                                window.location.reload();
                            }
                        });
    </script>

</body>

</html>
