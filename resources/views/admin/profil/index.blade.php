@extends('layout.master')

@section('content')
<div class="section-header">
  <h1>Update Profil</h1>
</div>
<div class="container">

  @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  <div class="section-body">
    <h2 class="section-title">Hi, Admin!</h2>
    <p class="section-lead">
      Change information about yourself on this page.
    </p>

    <div class="row mt-sm-4">
      <div class="col-12 col-md-12 col-lg-5">
        <div class="card profile-widget">
          <div class="profile-widget-header">
            <img alt="image" src="template1/dist/assets/img/avatar/avatar-1.png" class="rounded-circle profile-widget-picture">
          </div>
          <div class="profile-widget-description">
            <div class="profile-widget-name">Admin<div class="text-muted d-inline font-weight-normal">
                <div class="slash"></div> Admin
              </div>
            </div>
            This Is Admin MaBroo
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-7">
        <div class="card">

          <form action="{{ route('profil.update', ['user'=> $user->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-header">
              <h4>Edit Profile</h4>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="form-group col-md-10 ">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                </div>
              </div>


              <div class="row">
                <div class="form-group col-md-10 ">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-10">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password" value="{{ $user->password }}" required>
                  <input type="checkbox" onchange="showPassword()"> Show
                </div>
              </div>

              <div class="card-footer text-right">
                <button class="btn btn-primary">Save Changes</button>
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function showPassword() {
    var passwordField = document.getElementById("password");
    if (passwordField.type === "password") {
      passwordField.type = "text";
    } else {
      passwordField.type = "password";
    }
  }
</script>
@endsection