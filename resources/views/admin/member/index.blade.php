@extends('layout.master')
@section('content')
<div class="section-header">
    <h1>Daftar User</h1>
</div>
<div class="container">
    <a href="{{ route('member.create') }}" class="btn btn-primary mb-2">Tambah User</a>
    @if(session('success'))
    <div class="alert alert-success mt-2">
        {{ session('success') }}
    </div>
    @endif

    <table class="table mt-3">
        <thead>
            <tr style="text-align: center;">
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($members as $key => $member)
            <tr style="text-align: center;">
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $member->nama }}</td>
                <td>{{ $member->username }}</td>
                <td>{{ $member->email }}</td>
                <td>
                    <div class="d-flex justify-content-center">
                        <form action="" style="display: inline-block;">
                            <button type="button" class="btn btn-primary m-1 fas fa-eye" data-toggle="modal" data-target="#modal-detail" data-password="{{ $member->password }}" data-provinsi="{{ $member->provinsi }}" data-kota="{{ $member->kota }}" data-kecamatan="{{ $member->kecamatan }}" data-alamat="{{ $member->alamat_detail }}" data-nohp="{{ $member->no_hp }}"></button>
                        </form>

                        <form action="{{ route('member.edit', $member->id) }}" style="display: inline-block;">
                            <button type="submit" class="btn btn-warning m-1 fas fa-pen"></button>
                        </form>

                        <form id="formDelete{{ $member->id }}" action="{{ route('member.destroy', $member->id) }}" method="POST" style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-danger m-1 fas fa-trash delete-btn" data-id="{{ $member->id }}"></button>
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" style="text-align: center;">Tidak ada barang yang tersedia.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Tempatkan konten detail di sini -->
                <p><strong>Nama : </strong> <span id="detail-nama"></span></p>
                <p><strong>Username : </strong> <span id="detail-username"></span></p>
                <p><strong>Email : </strong> <span id="detail-email"></span></p>
                <p><strong>Provinsi : </strong> <span id="detail-provinsi"></span></p>
                <p><strong>Kota : </strong> <span id="detail-kota"></span></p>
                <p><strong>Kecamatan : </strong> <span id="detail-kecamatan"></span></p>
                <p><strong>Alamat : </strong> <span id="detail-alamat"></span></p>
                <p><strong>No Hp : </strong> <span id="detail-nohp"></span></p>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@push('js')
    

<script>
    $(document).ready(function() {
        // Menangkap event saat modal ditampilkan
        $('#modal-detail').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Tombol yang memicu modal
            var nama = button.closest('tr').find('td:eq(0)').text();
            var username = button.closest('tr').find('td:eq(1)').text();
            var email = button.closest('tr').find('td:eq(2)').text();
            var provinsi = button.data('provinsi');
            var kota = button.data('kota');
            var kecamatan = button.data('kecamatan');
            var alamat = button.data('alamat');
            var nohp = button.data('nohp');
            
            // Mengganti konten modal dengan informasi yang diambil
            $('#detail-nama').text(nama);
            $('#detail-username').text(username);
            $('#detail-email').text(email);
            $('#detail-provinsi').text(provinsi);
            $('#detail-kota').text(kota);
            $('#detail-kecamatan').text(kecamatan);
            $('#detail-alamat').text(alamat);
            $('#detail-nohp').text(nohp);
           
            // ... tambahkan penggantian informasi lainnya ...
        });
    });
</script>
<script>
    <?php if (isset($member)) { ?>

        $(document).ready(function() {
            // Temukan form berdasarkan ID
            var form = document.getElementById('formDelete{{ $member->id }}');

            // Tambahkan event listener untuk form submit
            $(form).submit(function(event) {
                event.preventDefault();

                // Lakukan pengajuan formulir menggunakan AJAX
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(response) {
                        // Tampilkan SweetAlert sukses
                        Swal.fire({
                            icon: 'success',
                            title: 'Data Berhasil DiHapus!',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((result) => {
                            // Redirect ke halaman tujuan (gantilah dengan URL yang sesuai)
                            window.location.href = "{{ route('member.index') }}";
                        });
                    },
                    error: function(error) {
                        // Tampilkan SweetAlert untuk kesalahan
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Terjadi kesalahan!',
                        });
                    }
                });
            });
        });
    <?php } ?>
</script>
@endpush