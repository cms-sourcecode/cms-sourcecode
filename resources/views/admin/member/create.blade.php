@extends('layout.master')

@section('content')
<div class="section-header">
    <h1>Tambah User</h1>
</div>
<div class="container">


    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form id="formCreate" action="{{ route('member.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" required>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" required>
            <input type="checkbox" onclick="showPassword()"> Show
        </div>
        <div class="form-group">
            <label for="provinsi">Provinsi</label>
            <input type="text" class="form-control" id="provinsi" name="provinsi" required>
        </div>
        <div class="form-group">
            <label for="kota">Kota</label>
            <input type="text" class="form-control" id="kota" name="kota" required>
        </div>
        <div class="form-group">
            <label for="kecamatan">Kecamatan</label>
            <input type="text" class="form-control" id="kecamatan" name="kecamatan" required>
        </div>
        <div class="form-group">
            <label for="alamat_detail">Alamat</label>
            <input type="text" class="form-control" id="alamat_detail" name="alamat_detail" required>
        </div>
        <div class="form-group">
            <label for="no_hp">No HP</label>
            <input type="number" class="form-control" id="no_hp" name="no_hp" required>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
    <script>
        function showPassword() {
            var passwordField = document.getElementById("password");
            if (passwordField.type === "password") {
                passwordField.type = "text";
            } else {
                passwordField.type = "password";
            }
        }
    </script>
</div>


<!-- SweetAlert CDN -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<!-- jQuery CDN -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- Sweet Alert -->
<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Temukan form berdasarkan ID
        var form = document.getElementById('formCreate');

        // Tambahkan event listener untuk form submit
        $(form).submit(function(event) {
            event.preventDefault();

            // Lakukan pengajuan formulir menggunakan AJAX
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    // Tampilkan SweetAlert sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Data Berhasil Ditambahkan!',
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        // Redirect ke halaman tujuan (gantilah dengan URL yang sesuai)
                        window.location.href = "{{ route('member.index') }}";
                    });
                },
                error: function(error) {
                    // Tampilkan SweetAlert untuk kesalahan
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi kesalahan!',
                    });
                }
            });
        });
    });
</script>
@endsection