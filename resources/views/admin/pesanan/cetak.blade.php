<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        table.static {
            position: relative;
            border: 1px;
        }
    </style>
    <title> Data Pesanan</title>
</head>

<body>

    <form action="{{ route('pesanan.cetak') }}" method="get">
        @csrf
        <label for="start_date">Tanggal Mulai:</label>
        <input type="date" id="start_date" name="start_date" required>

        <label for="end_date">Tanggal Selesai:</label>
        <input type="date" id="end_date" name="end_date" required>

        <button type="submit">Cetak</button>
    </form>

    <div class="form-group">
        <p align=center> <b>Laporan Data Pesanan</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width: 95%">

            <thead>
                <tr>
                    <th style="text-align: center; font-weight:bold">No</th>
                    <th style="text-align: center; font-weight:bold">Tanggal</th>
                    <th style="text-align: center; font-weight:bold">Member</th>
                    <th style="text-align: center; font-weight:bold">Barang</th>
                    <th style="text-align: center; font-weight:bold">Harga Jual</th>
                    <th style="text-align: center; font-weight:bold">Keuntungan</th>
                    <th style="text-align: center; font-weight:bold">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse($orders as $key => $order)
                <tr>
                    <th style="text-align: center;" scope="row">{{ $key + 1 }}</th>
                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($order['created_at'])) }}</td>
                    <td style="text-align: center;">{{ $order['member']['nama'] }}</td>
                    <td style="text-align: center;">{{ $order['barang']['nama_produk'] }}</td>
                    <td style="text-align: center;">{{ 'Rp ' . number_format($order['barang']['harga_jual']) }}</td>
                    <td style="text-align: center;">{{ 'Rp ' . number_format($order['barang']['keuntungan']) }}</td>
                    <td></td>
                </tr>
                @empty
                <tr>
                    <td colspan="8" style="text-align: center;">Tidak ada barang yang tersedia.</td>
                </tr>
                @endforelse
            </tbody>

        </table>
    </div>

    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>