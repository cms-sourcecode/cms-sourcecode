<!-- resources/views/barang/index.blade.php -->

@extends('layout.master')
@section('content')

<div class="section-header">
    <h1>Recap Pesanan</h1>
</div>
<div class="container">

    <form action="filter" method="get">
        <div class="row pb-3">

            <div class="col md-3">
                <label> Start Date: </label>
                <input type="date" name="start_date" class="form-control">
            </div>

            <div class="col md-3">
                <label> End Date:</label>
                <input type="date" name="end_date" class="form-control">
            </div>
            <div class="col md-1 pt-4">
                <button type="submit" class="btn btn-primary"> Filter </button>
            </div>
        </div>
    </form>

    <div>
        <a href="{{ route('pesanan.cetak') }}" target="_blank" class="btn btn-primary fas fa-print"> Cetak</a>

    </div>


    <table class="table mt-3">
        <thead>
            <tr>
                <th style="text-align: center; font-weight:bold">No</th>
                <th style="text-align: center; font-weight:bold">Tanggal</th>
                <th style="text-align: center; font-weight:bold">Pembeli</th>
                <th style="text-align: center; font-weight:bold">Barang</th>
                <th style="text-align: center; font-weight:bold">Harga Jual</th>
                <th style="text-align: center; font-weight:bold">Keuntungan</th>
                <th style="text-align: center; font-weight:bold">Bukti Struk</th>
                <th style="text-align: center; font-weight:bold">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($orders as $key => $order)
            <tr>
                <th style="text-align: center;" scope="row">{{ $key + 1 }}</th>
                <td style="text-align: center;">{{ date('d-m-Y', strtotime($order['created_at'])) }}</td>
                <td style="text-align: center;">{{ $order['member']['nama'] }}</td>
                <td style="text-align: center;">{{ $order['barang']['nama_produk'] }}</td>
                <td style="text-align: center;">{{ 'Rp ' . number_format($order['barang']['harga_jual']) }}</td>
                <td style="text-align: center;">{{ 'Rp ' . number_format($order['barang']['keuntungan']) }}</td>
                <td style="text-align: center;">
                    @if ($order['bukti_pembayaran'])
                    <a href="{{ asset('bukti/' . $order['bukti_pembayaran']) }}" target="_blank">
                        <img src="{{ asset('bukti/' . $order['bukti_pembayaran']) }}" alt="Bukti Struk" style="max-width: 100px; max-height: 100px;">
                    </a>
                    @else
                    Tidak ada bukti struk
                    @endif
                </td>
                <td style="text-align: center;">
                    @if ($order['bukti_pembayaran_status'] == 'uploaded')
                    <form action="{{ route('pesanan.konfirmasi', ['id' => $order['id']]) }}" method="post" style="display: inline;">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-success mb-2">Konfirmasi Pembayaran</button>
                    </form>
                    <form action="{{ route('pesanan.batalkan', ['id' => $order['id']]) }}" method="post" style="display: inline;">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-danger">Batalkan Pembayaran</button>
                    </form>
                    @else
                    Pesanan Telah Dikonfirmasi
                    @endif
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="8" style="text-align: center;">Tidak ada barang yang tersedia.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

@endsection