@extends('layout.master')
@section('content')
<div class="section-header">
    <h1>Update Produk</h1>
</div>
<div class="container">


    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form id="formEdit" action="{{ route('barang.update', $barang->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="categories_id">Kategori</label>
            <select class="form-control" id="categories_id" name="categories_id" required>
                @foreach($categories as $category)
                <option value="{{ $category->id }}" {{ $barang->categories_id == $category->id ? 'selected' : '' }}>{{ $category->nama_kategori }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="nama_produk">Nama Produk</label>
            <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="{{ $barang->nama_produk }}" required>
        </div>

        <div class="form-group">
            <label for="id_member">Author</label>
            <select class="form-control" id="id_member" name="id_member" required>
                @foreach($members as $member)
                <option value="{{ $member->id }}" {{ $barang->id_member == $member->id ? 'selected' : '' }}>{{ $member->nama }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="deskripsi_produk">Deskripsi</label>
            <textarea class="form-control" id="deskripsi_produk" name="deskripsi_produk" rows="4" required>{{ $barang->deskripsi_produk }}</textarea>
        </div>
        <div class="form-group">
            <label for="gambar_produk">Gambar Produk</label>
            <input type="file" class="form-control-file" id="gambar_produk" name="gambar_produk" accept="image/*">
            <img src="{{ asset('uploads/' . $barang->gambar_produk) }}" alt="Gambar Produk" style="max-width: 75%;">
        </div>
        <div class="form-group">
            <label for="harga_modal">Harga Modal</label>
            <input type="number" class="form-control" id="harga_modal" name="harga_modal" value="{{ $barang->harga_modal }}" required>
        </div>

        <div class="form-group">
            <label for="file_produk">File</label>
            <input type="file" class="form-control-file" id="file_produk" name="file_produk" accept="application/zip, application/x-rar-compressed">
            <img src="{{ asset('files/' . $barang->file_produk) }}">
        </div>
        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </form>
</div>

<!-- Sweet Alert -->
<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Temukan form berdasarkan ID
        var form = document.getElementById('formEdit');

        // Tambahkan event listener untuk form submit
        $(form).submit(function(event) {
            event.preventDefault();

            // Lakukan pengajuan formulir menggunakan AJAX
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    // Tampilkan SweetAlert sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Data Berhasil DiUpdate!',
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        // Redirect ke halaman tujuan (gantilah dengan URL yang sesuai)
                        window.location.href = "{{ route('barang.index') }}";
                    });
                },
                error: function(error) {
                    // Tampilkan SweetAlert untuk kesalahan
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi kesalahan!',
                    });
                }
            });
        });
    });
</script>
@endsection