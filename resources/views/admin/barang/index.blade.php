@extends('layout.master')
@section('content')

<div class="section-header">
    <h1>Daftar Produk</h1>
</div>
<div class="container">

    <a href="{{ route('barang.create') }}" class="btn btn-primary">Tambah Produk</a>

    @if(session('success'))
    <div class="alert alert-success mt-2">
        {{ session('success') }}
    </div>
    @endif

    <table class="table mt-3 table-responsive">
        <thead>
            <tr>
                <th style="text-align: center; font-weight:bold">No</th>
                <th style="text-align: center; font-weight:bold">Kategori</th>
                <th style="text-align: center; font-weight:bold">Nama Produk</th>
                <th style="text-align: center; font-weight:bold">Author</th>
                <th style="text-align: center; font-weight:bold">Harga Modal</th>
                <th style="text-align: center; font-weight:bold">Harga Jual</th>
                <th style="text-align: center; font-weight:bold">Keuntungan</th>
                <th style="text-align: center; font-weight:bold">Tanggal Publish</th>
                <th style="text-align: center; font-weight:bold">Status</th>
                <th style="text-align: center; font-weight:bold">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($barangs as $key => $barang)
            <tr>
                <th style="text-align: center;">{{ $key + 1 }}</th>
                <td style="text-align: center;">{{ optional($barang->kategori)->nama_kategori }}</td>

                <td style="text-align: center;">{{ $barang->nama_produk }}</td>
                <td style="">{{ optional($barang->member)->nama }}</td>
                <td style="">Rp {{ number_format($barang->harga_modal, 2) }}</td>
                <td style="">Rp {{ number_format($barang->harga_jual, 2) }}</td>
                <td style="">Rp {{ number_format($barang->keuntungan, 2) }}</td>
                <td style="">{{ $barang->tgl_publish }}</td>
                <td style="text-align: center;">
                    <div class="form-group">
                        <form action="{{ route('barang.update-status', $barang->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <label for="status"></label>
                            <select style="font-size: 13px; padding: 9px;" id="status" name="status" onchange="this.form.submit()">
                                <option value="aktif" {{ $barang->status === 'aktif' ? 'selected' : '' }}>Aktif</option>
                                <option value="tidak aktif" {{ $barang->status === 'tidak aktif' ? 'selected' : '' }}>Tidak Aktif</option>
                            </select>
                        </form>
                    </div>
                </td>
                <td style="text-align: center;">

                    <div class="d-flex justify-content-center">
                        <form action="" style="display: inline-block;">
                            <button type="button" class="btn btn-primary m-1 fas fa-eye" data-toggle="modal" data-target="#modal-detail" data-deskripsi-produk="{{ $barang->deskripsi_produk }}" data-gambar="{{ asset('uploads/' . $barang->gambar_produk) }}" data-file-produk="{{ $barang->file_produk }}" data-buat="{{ $barang->created_at }}" data-publish="{{ $barang->tgl_publish }}" data-status="{{ $barang->status }}"></button>
                        </form>

                        <form action="{{ route('barang.edit', $barang->id) }}" style="display: inline-block;">
                            <button type="submit" class="btn btn-warning m-1 fas fa-pen"></button>
                        </form>

                        <!-- resources/views/barang/index.blade.php -->
                        <!-- Tambahkan ID unik untuk setiap formulir -->
                        <form id="formDelete{{ $barang->id }}" action="{{ route('barang.destroy', $barang->id) }}" method="POST" style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-danger m-1 fas fa-trash delete-btn" data-id="{{ $barang->id }}"></button>
                        </form>


                    </div>
                </td>

            </tr>

            @empty
            <tr>
                <td colspan="8" style="text-align: center;">Tidak ada barang yang tersedia.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection

<!-- Tambahkan modal di akhir konten Anda -->
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Tempatkan konten detail di sini -->
                <p><strong>Kategori : </strong> <span id="detail-kategori"></span></p>
                <p><strong>Nama Produk : </strong> <span id="detail-nama"></span></p>
                <p><strong>Author : </strong> <span id="detail-member"></span></p>
                <p><strong>Deskripsi : </strong> <span id="detail-deskripsi"></span></p>
                <p><br><strong>Gambar : <br></strong> <img id="detail-gambar" style="max-width: 200px; max-height: 200px;" alt="Gambar Produk"></p>
                <p><br><strong>Harga Modal : </strong> <span id="detail-modal"></span></p>
                <p><strong>Harga Jual : </strong> <span id="detail-jual"></span></p>
                <p><strong>Keuntungan : </strong> <span id="detail-keuntungan"></span></p>
                <p><strong>File : </strong> <span id="detail-file"></span></p>
                <p><strong>Tanggal Buat : </strong> <span id="detail-buat"></span></p>
                <p><strong>Tanggal Publish : </strong> <span id="detail-publish"></span></p>
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@push('js')

<script>
    $(document).ready(function() {
        // Menangkap event saat modal ditampilkan
        $('#modal-detail').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget); // Tombol yang memicu modal
            var kategori = button.closest('tr').find('td:eq(0)').text();
            var nama = button.closest('tr').find('td:eq(1)').text();
            var member = button.closest('tr').find('td:eq(2)').text();
            var deskripsi = button.data('deskripsi-produk');
            var gambar = button.data('gambar');
            var modal = button.closest('tr').find('td:eq(3)').text();
            var jual = button.closest('tr').find('td:eq(4)').text();
            var keuntungan = button.closest('tr').find('td:eq(5)').text();
            var file = button.data('file-produk');
            var buat = button.data('buat');
            var publish = button.closest('tr').find('td:eq(6)').text();
         

            // Mengganti konten modal dengan informasi yang diambil
            $('#detail-kategori').text(kategori);
            $('#detail-nama').text(nama);
            $('#detail-member').text(member);
            $('#detail-deskripsi').text(deskripsi);
            $('#detail-gambar').attr('src', gambar);
            $('#detail-modal').text(modal);
            $('#detail-jual').text(jual);
            $('#detail-keuntungan').text(keuntungan);
            $('#detail-file').text(file);
            $('#detail-buat').text(buat);
            $('#detail-publish').text(publish);
          
            
        });
    });
</script>
<!-- SWEET ALERT DELETE -->

<script>
    $(document).ready(function() {
        // Menangkap event saat tombol delete diklik
        $('.delete-btn').on('click', function() {
            var id = $(this).data('id');
            // Tampilkan Sweet Alert untuk konfirmasi penghapusan
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: 'Anda tidak akan dapat mengembalikan ini!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                // Jika tombol "Ya, hapus!" diklik
                if (result.isConfirmed) {
                    // Kirim formulir penghapusan
                    $(`form[action="{{ route('barang.destroy', '') }}/${id}"]`).submit();
                }
            });
        });
    });
</script>


<script>
    <?php if (isset($barang)) { ?>

        $(document).ready(function() {
            // Temukan form berdasarkan ID
            var form = document.getElementById('formDelete{{ $barang->id }}');

            // Tambahkan event listener untuk form submit
            $(form).submit(function(event) {
                event.preventDefault();

                // Lakukan pengajuan formulir menggunakan AJAX
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(response) {
                        // Tampilkan SweetAlert sukses
                        Swal.fire({
                            icon: 'success',
                            title: 'Data Berhasil DiHapus!',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((result) => {
                            // Redirect ke halaman tujuan (gantilah dengan URL yang sesuai)
                            window.location.href = "{{ route('barang.index') }}";
                        });
                    },
                    error: function(error) {
                        // Tampilkan SweetAlert untuk kesalahan
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Terjadi kesalahan!',
                        });
                    }
                });
            });
        });
    <?php } ?>
</script>
@endpush
