<!-- General JS Scripts -->
<script src="{{asset('template1/dist/assets/modules/jquery.min.js')}}"></script>
<script src="{{asset('template1/dist/assets/modules/popper.js')}}"></script>
<script src="{{asset('template1/dist/assets/modules/tooltip.js')}}"></script>
<script src="{{asset('template1/dist/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('template1/dist/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('template1/dist/assets/modules/moment.min.js')}}"></script>
<script src="{{asset('template1/dist/assets/js/stisla.js')}}"></script>

<!-- JS Libraies -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- Page Specific JS File -->

<!-- Template JS File -->
<script src="{{asset('template1/dist/assets/js/scripts.js')}}"></script>
<script src="{{asset('template1/dist/assets/js/custom.js')}}"></script>

<script src="{{asset('template1/dist/assets/modules/summernote/summernote-bs4.js') }}"></script>
{{-- 
<script src="{{asset ('template/assets/modules/sweetalert/sweetalert.min.js') }}"></script>
<script src="assets/js/page/modules-sweetalert.js"></script> --}}
@stack('js')
