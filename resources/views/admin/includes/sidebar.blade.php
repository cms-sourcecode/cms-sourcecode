<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">SOURCECODE</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">St</a>
        </div>
        <ul class="sidebar-menu">

            <li class="menu-header">Dashboard</li>
            <li class="dropdown">
                <a href="{{ route('dashboard.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">Menu</li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-pencil-ruler"></i><span>Menu</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('kategori.index') }}">Daftar Kategori</a></li>
                    <li><a class="nav-link" href="{{ route('barang.index') }}">Daftar Produk</a></li>
                </ul>

           
            <li class="menu-header">Recap Laporan</li>
            <li class="dropdown">
                <a href="{{ route('pesanan.index') }}" class="nav-link "><i class="fas fa-file-alt"></i><span class="menu-title">Recap Pesanan</span></a>
            </li>

            <li class="menu-header">Daftar User</li>
            <li class="dropdown">
                <a href="{{ route('member.index') }}" class="nav-link"><i class="fas fa-user"></i><span>Daftar User</span></a>
            </li>
           
        </ul>

    </aside>
</div>