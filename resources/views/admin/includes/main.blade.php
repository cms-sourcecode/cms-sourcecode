<div class="main-content">
    <section class="section">


        <div class="section-body">
            @yield('content')
        </div>
    </section>
</div>