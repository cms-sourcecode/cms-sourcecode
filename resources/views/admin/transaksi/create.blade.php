@extends('layout.master')

@section('content')
<div class="section-header">
    <h1>Tambah Daftar Transaksi</h1>
</div>

<form action="{{ route('transaksi.store') }}" method="POST">
    @csrf

    <div class="form-group">
        <label for="tanggal">Tanggal</label>
        <input type="date" class="form-control" id="tanggal" name="tanggal" required>
    </div>

    <div class="form-group">
        <label for="nama_barang">Nama Barang</label>
        <input type="text" class="form-control" id="nama_barang" name="nama_barang" required>
    </div>

    <div class="form-group">
        <label for="jumlah_order">Jumlah Order</label>
        <input type="number" class="form-control" id="jumlah_order" name="jumlah_order" required>
    </div>

    <div class="form-group">
        <label for="jumlah_harga">Jumlah Harga</label>
        <input type="text" class="form-control" id="jumlah_harga" name="jumlah_harga" required>
    </div>

    <div class="form-group">
        <label for="no_rekening">Nomor Rekening</label>
        <input type="text" class="form-control" id="no_rekening" name="no_rekening" required>
    </div>

    <div class="form-group">
        <label for="nama_pembeli">Atas Nama</label>
        <input type="text" class="form-control" id="nama_pembeli" name="nama_pembeli" required>
    </div>

    <div class="form-group">
        <label for="status">Status</label>
        <select name="status" id="status" class="form-control" required>
            <option value="DITERIMA">Diterima</option>
            <option value="DITOLAK">Ditolak</option>
            <option value="MENUNGGU">Menunggu</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
</div>
@endsection