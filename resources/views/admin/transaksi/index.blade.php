@extends('layout.master') 
@section('content')
<div class="section-header">
    <h1>Daftar Transaksi</h1>
</div>

<div class="container">
    <a href="{{ route('transaksi.create') }}" class="btn btn-primary mb-2">Tambah Transaksi</a>
    @if(session('success'))
    <div class="alert alert-success mt-2">
        {{ session('success') }}
    </div>
    @endif
    <table class="table">
        <thead>
            <tr style="background-color: #87C4FF;">
                <th>No</th>
                <th>Tanggal</th>
                <th>Nama Barang</th>
                <th>Jumlah Order</th>
                <th>Jumlah Harga</th>
                <th>Nomor Rekening</th>
                <th>Nama Pembeli</th>
                <th>Status</th>

                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($transaksis as $transaksi)
            <tr>
                <td>{{ $transaksi->id }}</td>
                <td>{{ $transaksi->tanggal }}</td>
                <td>{{ $transaksi->nama_barang }}</td>
                <td>{{ $transaksi->jumlah_order }}</td>
                <td>{{ $transaksi->jumlah_harga }}</td>
                <td>{{ $transaksi->no_rekening }}</td>
                <td>{{ $transaksi->nama_pembeli }}</td>
                <td>{{ $transaksi->status }}</td>

                <td>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('transaksi.edit', $transaksi->id) }}" class="btn btn-warning btn-sm m-1">Edit</a>
                        <form action="{{ route('transaksi.destroy', $transaksi->id) }}" method="POST" style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm m-1" onclick="return confirm('Apakah Anda yakin ingin menghapus transaksi ini?')">Hapus</button>
                        </form>

                    </div>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="8" style="text-align: center;">Tidak ada barang yang tersedia.</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection