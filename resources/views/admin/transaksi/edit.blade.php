<!-- resources/views/transaksis/edit.blade.php -->

@extends('layout.master')

@section('content')


<div class="section-header">
    <h1>Edit Transaksi</h1>
</div>

<div class="container">


    <form action="{{ route('transaksi.update', $transaksi->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="tanggal">Tanggal</label>
            <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $transaksi->tanggal }}" required>
        </div>

        <div class="form-group">
            <label for="nama_barang">Nama Barang</label>
            <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="{{ $transaksi->nama_barang }}" required>
        </div>

        <div class="form-group">
            <label for="jumlah_order">Jumlah Order</label>
            <input type="number" class="form-control" id="jumlah_order" name="jumlah_order" value="{{ $transaksi->jumlah_order }}" required>
        </div>

        <div class="form-group">
            <label for="jumlah_harga">Jumlah Harga</label>
            <input type="text" class="form-control" id="jumlah_harga" name="jumlah_harga" value="{{ $transaksi->jumlah_harga }}" required>
        </div>

        <div class="form-group">
            <label for="no_rekening">Nomor Rekening</label>
            <input type="text" class="form-control" id="no_rekening" name="no_rekening" value="{{ $transaksi->no_rekening }}" required>
        </div>

        <div class="form-group">
            <label for="nama_pembeli">Atas Nama</label>
            <input type="text" class="form-control" id="nama_pembeli" name="nama_pembeli" value="{{ $transaksi->nama_pembeli }}" required>
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control" required>
                <option value="DITERIMA">Diterima</option>
                <option value="DITOLAK">Ditolak</option>
                <option value="MENUNGGU">Menunggu</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </form>
</div>
@endsection