@extends('layout.master')
@section('content')

<div class="section-header">
    <h1>Edit Kategori</h1>
</div>
<div class="container">


    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form id="formEdit" action="{{ route('kategori.update', $kategori->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" value="{{ $kategori->nama_kategori }}" required>
        </div>

        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
    </form>
</div>

<!-- Sweet Alert -->
<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Temukan form berdasarkan ID
        var form = document.getElementById('formEdit');

        // Tambahkan event listener untuk form submit
        $(form).submit(function(event) {
            event.preventDefault();

            // Lakukan pengajuan formulir menggunakan AJAX
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    // Tampilkan SweetAlert sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Data Berhasil DiUpdate!',
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        // Redirect ke halaman tujuan (gantilah dengan URL yang sesuai)
                        window.location.href = "{{ route('kategori.index') }}";
                    });
                },
                error: function(error) {
                    // Tampilkan SweetAlert untuk kesalahan
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi kesalahan!',
                    });
                }
            });
        });
    });
</script>
@endsection