@extends('layout.master')
@section('content')

<div class="section-header">
    <h1>Daftar Kategori</h1>
</div>
<div class="container">

    <a href="{{ route('kategori.create') }}" class="btn btn-primary mb-2">Tambah Kategori</a>

    @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <table class="table">
        <thead>
            <tr style="text-align: center; background-color: #87C4FF;">
                <th scope="col">No</th>
                <th scope="col">Nama Kategori</th>

                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($kategori as $key => $item)
            <tr style="text-align: center;">
                <th>{{ $key + 1 }}</th>
                <td>{{ $item->nama_kategori }}</td>

                <td>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('kategori.edit', $item->id) }}" class="btn btn-sm btn-warning m-1">Edit</a>
                        <form id="formDelete{{ $item->id }}" action="{{ route('kategori.destroy', $item->id) }}" method="POST" style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-sm btn-danger m-1 delete-btn" data-id="{{ $item->id }}">Hapus</button>
                            <!--  -->
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" style="text-align: center;">Tidak ada barang yang tersedia.</td>
            </tr>

            @endforelse

        </tbody>
    </table>
</div>
@endsection

<!-- SWEET ALERT DELETE -->
@push('js')
<script>
    $(document).ready(function() {
        // Menangkap event saat tombol delete diklik
        $('.delete-btn').on('click', function() {
            var id = $(this).data('id');
            // Tampilkan Sweet Alert untuk konfirmasi penghapusan
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: 'Anda tidak akan dapat mengembalikan ini!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                // Jika tombol "Ya, hapus!" diklik
                if (result.isConfirmed) {
                    // Kirim formulir penghapusan
                    $(`form#formDelete${id}`).submit();
                }
            });
        });
    });
</script>


<script>
    $(document).ready(function() {
        // Menangkap event saat modal ditampilkan
        $('#modal-detail').on('show.bs.modal', function(event) {
            // ... (your existing modal script)
        });
    });
</script>

<script>
    <?php if (isset($item)) { ?>

        $(document).ready(function() {
            // Temukan form berdasarkan ID
            var form = document.getElementById('formDelete{{ $item->id }}');

            // Tambahkan event listener untuk form submit
            $(form).submit(function(event) {
                event.preventDefault();

                // Lakukan pengajuan formulir menggunakan AJAX
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(response) {
                        // Tampilkan SweetAlert sukses
                        Swal.fire({
                            icon: 'success',
                            title: 'Kategori Berhasil DiHapus!',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((result) => {
                            // Redirect ke halaman tujuan (gantilah dengan URL yang sesuai)
                            window.location.href = "{{ route('kategori.index') }}";
                        });
                    },
                    error: function(error) {
                        // Tampilkan SweetAlert untuk kesalahan
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Terjadi kesalahan!',
                        });
                    }
                });
            });
        });
    <?php } ?>
</script>
@endpush