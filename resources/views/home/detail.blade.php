@extends('layout.masteruser')
    @section('content')


<div class="container custom-small-container`">
    <div class="row">
        <!-- Kolom pertama (8 bagian) -->
        <div class="col-md-8">
            <h1 class="card-title mb-6">{{ $barangs->nama_produk}}</h1>
            <img class="card-img-top" style="max-width:75%" src="{{ url('uploads')}}/{{$barangs->gambar_produk}}" alt="Product Image">
            <div class="card-body mb-5">

                <p class="card-text" mb-3><a href="#" class="btn btn-light"><strong>{{ $barangs->kategori->nama_kategori}}</strong></a></p>
                <p class="card-text" style="text-align: justify;">{{ $barangs->deskripsi_produk }}</p>
                <p class="card-text">Price: Rp.{{ $barangs->harga_jual }}</p>
                <p class="card-text">Author: {{ ($barangs->member)->nama }} </p>

            </div>
        </div><br><br>

        <!-- hiasan kanan atas -->
        <div class="col-12 col-sm-4 col-lg-4 mt-5">
            <div>
                <div class="card card-secondary" style="text-align: center;">
                    <div class="card-header ui-sortable-handle">
                        <h4>Price: Rp.{{ $barangs->harga_jual }}</h4>
                    </div>
                    <div class="card-body">
                        <!-- Isi card body sesuai kebutuhan Anda -->

                        <!-- Tombol Pembayaran -->
                        <a href="#" class="btn btn-primary">Bayar Sekarang</a>
                    </div>
                </div>

            <!-- hiasan di kanan bawah -->
            <div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        Version:
                    </div>
                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-600">
                        <span>v1.0.0</span>
                    </div>
                </div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        <span>Update Date:</span>
                    </div>
                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-600">
                        <span>2 months ago</span>
                    </div>
                </div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        <span>Published Date:</span>
                    </div>
                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-600">
                        <span>{{ $barangs->tanggal_publish }}</span>
                    </div>
                </div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        <span>Category:</span>
                    </div>
<div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-900 ">
                            <span>{{ $barangs->kategori->nama_kategori}}</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>
</div>

@endsection
