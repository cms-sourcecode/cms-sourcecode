<!-- FOOTER -->
    
<footer>

<div class="bootscore-footer bg-dark pt-5 pb-3 text-light">
  <div class="container">

    <!-- Top Footer Widget -->

    <div class="row">

      <!-- Footer 1 Widget -->
      <div class="col-md-6 col-lg-3">
        <div>
          <div class="footer_widget mb-4">
            <a href="index.html" class="navbar-brand sidebar-gone-hide ml-5">
              <img src="{{ asset('template1/dist/assets/img/logo.jpg') }}" alt="Logo" width="100" height="100">

            </a>
            <h2 class="widget-title ml-1 ">SourceCode</h2>
            <div class="textwidget">

            </div>
          </div>
        </div>
      </div>

      <!-- Footer 2 Widget -->
      <div class="col-md-6 col-lg-3">
        <div>
          <div class="footer_widget mb-4">
            <h2 class="widget-title h4">Temukan Kami</h2>
            <div class="menu-social-menu-container">
              <ul id="menu-social-menu" class="menu" style="list-style-type: none;">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1485">
                  <a href="https://www.instagram.com/garudacyber/">
                    <i class="fab fa-instagram"></i> Instagram
                  </a>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1486">
                  <a href="#">
                    <i class="fab fa-facebook"></i> Facebook
                  </a>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1487">
                  <a href="https://www.youtube.com/@GarudaCyberIndonesia">
                    <i class="fab fa-youtube"></i> Youtube
                  </a>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1488">
                  <a href="mailto:dewiparapatindahsari@gmail.com">
                    <i class="far fa-envelope"></i> Email
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>


      <!-- Footer 3 Widget -->
      <div class="col-md-6 col-lg-3">
        <div>
          <div class="footer_widget mb-4">
            <h2 class="widget-title h4">Hubungi Kami</h2>
            <div class="textwidget">
              <ul class="list-unstyled">
                <li><i class="fa fa-map-marker"></i> Jl. HR. Soebrantas, Sidomulyo Bar., Kec. Tampan, Kota Pekanbaru, Riau 28293</li>
                <li><a href="mailto:dewiparapatindahsari@gmail.com"><i class="fa fa-envelope"></i>&nbsp;dewiparapatindahsari@gmail.com<br>
                  </a></li>
                <li><a href="https://wa.me/6285737058375?text=Halo+Herdi%2C+Saya+ingin+memesan+source+code..."><i class="fa fa-phone"></i>&nbsp;085737058375<br>
                  </a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <!-- Footer 4 Widget -->
      <div class="col-md-6 col-lg-3">
        <div>
          <div class="footer_widget mb-4">
            <h2 class="widget-title h4">Tentang Kami</h2>
            <div class="textwidget">
              <p style="text-align:justify"><strong>SourceCode</strong> merupakan website media online yang menyediakan source code program aplikasi gratis dan berbayar. </p>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer Widgets End -->

    </div>

    <!-- Bootstrap 5 Nav Walker Footer Menu -->
    <ul id="footer-menu" class="nav ">
      <li id="menu-item-1890" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item nav-item-1890"><a href="{{ route('home.index') }}" class="nav-link active">Beranda</a></li>
      <li id="menu-item-1309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy nav-item nav-item-1309"><a href="#" class="nav-link ">Privacy Policy</a></li>
      <li id="menu-item-1310" class="menu-item menu-item-type-post_type menu-item-object-page nav-item nav-item-1310"><a href="#" class="nav-link ">About</a></li>
      <li id="menu-item-1311" class="menu-item menu-item-type-post_type menu-item-object-page nav-item nav-item-1311"><a href="#" class="nav-link ">Kontak</a></li>
    </ul> <!-- Bootstrap 5 Nav Walker Footer Menu End -->

  </div>
  <!-- FOOTER -->
</div>

</footer>