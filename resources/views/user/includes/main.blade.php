<div class="main-content">
  <section class="section">




    <!-- Pencarian -->
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <form action="{{ route('search') }}" method="GET">
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Cari produk..." name="query">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Cari</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>



    <div class="py-3 py-md-5  text-center">
      <h3 class="display-3">SourceCode</h3>
      <p class="lead">Selamat Datang di SourceCode - Sumber Terbaik untuk Source Code!</p>
    </div>


    @yield('content')

  </section>
</div>