  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('template1/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template1/dist/assets/modules/fontawesome/css/all.min.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('template1/dist/assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('template1/dist/assets/css/components.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha384-GLhlTQ8iow4f4tPzv+L6+ftCIxlXn1u1Zl2g/vLqUuQ" crossorigin="anonymous" />

  <!-- Sweet Alert -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <!-- Favicon -->
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <title>SourceCode</title>

  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->

  <!-- Your Custom CSS -->
  <style>
    /* Menetapkan lebar maksimum agar tata letak responsif */
    .container {
      max-width: 1200px;

    }





    /* Mengatur tata letak navbar */
    .navbar {
      display: flex;
      justify-content: space-between;
      align-items: center;



    }

    .navbar-bg {
      height: 70px;
      width: 100%;
    }



    /* Mengatur tata letak footer */
    .main-footer {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }



    .navbar-nav .nav-item {
      margin-right: 20px;
    }

    .navbar-nav .nav-link {
      color: #fff;
      font-weight: bold;
      font-size: 15px;
    }

    .navbar-nav .nav-link:hover {
      color: #001B79;
      transform: scale(1);
      font-size: 16px;
      /* Sesuaikan sesuai kebutuhan */
    }

    .navbar-brand {
      margin-top: 10px;
      font-size: 25px;
    }

    .bootscore-footer a,
    .bootscore-footer a:visited {
      color: #ffffff;
      /* Warna tulisan link normal dan yang sudah dikunjungi */
    }

    .bootscore-footer a:hover,
    .bootscore-footer a:focus {
      color: #7071E8;
      /* Warna tulisan link saat dihover atau difokuskan */
      text-decoration: none;
      /* Menghapus garis bawah pada hover atau focus */
    }

    <style>
    .card-date {
        font-size: smaller;
        font-weight: normal;
    }

    .pagination {
        display: flex;
        justify-content: center;
        margin-top: 20px;
        /* Sesuaikan dengan jarak yang diinginkan */
    }

    .pagination li {
        margin: 0 5px;
        /* Sesuaikan dengan jarak antara elemen pagination */
        font-size: 14px;
        /* Sesuaikan dengan ukuran font yang diinginkan */
    }

    .pagination .page-link {
        padding: 8px 12px;
        /* Sesuaikan dengan ukuran padding yang diinginkan */
    }

    .card-body {
        text-align: justify;
    }

    .card-title {
        font-size: 18px;
        font-weight: bold;
    }

    .card-description {
        margin-top: 5px;
        margin-bottom: 10px;
    }
    .card-img-top {
        object-fit: cover;
        height: 200px;
        width: 100%;
        border-radius: 10px;
    }

  </style>
