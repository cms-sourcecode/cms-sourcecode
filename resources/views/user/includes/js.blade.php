<script>
  function getBarangByCategory(categoryId) {
    // Ajax request to get barang by category id
    $.ajax({
      url: '/get-barang-by-category/' + categoryId,
      type: 'GET',
      success: function(data) {
        // Handle the response data (e.g., update the UI with barang data)
        console.log(data);
      },
      error: function(error) {
        console.error('Error fetching barang by category:', error);
      }
    });
  }
</script>
<script src="{{ asset('template1/dist/assets/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/modules/popper.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/modules/tooltip.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/modules/moment.min.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/js/stisla.js') }}"></script>

  <!-- JS Libraies -->

  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="{{ asset('template1/dist/assets/js/scripts.js') }}"></script>
  <script src="{{ asset('template1/dist/assets/js/custom.js') }}"></script>