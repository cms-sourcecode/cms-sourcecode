<style>
  .main-navbar {
    margin-bottom: 20px;
  }
</style>


<nav class="navbar navbar-expand-lg main-navbar ">


  <a href="index.html" class="navbar-brand sidebar-gone-hide">
    <img src="{{ asset('template1/dist/assets/img/fixlogonobg.png') }}" alt="Logo" width="50" height="50">
    SOURCECODE
  </a>
  <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar">
    <i class="fas fa-bars"></i>
  </a>
  <div class="nav-collapse">
    <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
      <i class="fas fa-ellipsis-v"></i>
    </a>
    <!-- navbar menu -->
    <ul class="navbar-nav menu  ">
      <li class="nav-item"><a href="{{ route('home.index') }}" class="nav-link ">Home</a></li>
      <li class="nav-item dropdown d-inline mr-2 show">

        <div class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kategori
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @php
              $categories = App\Models\Kategori::all(); 
            @endphp
           @foreach ($categories as $category)
           <a class="dropdown-item" href="{{ route('produk.by.kategori', ['nama_kategori' => $category->id]) }}">{{ $category->nama_kategori }}</a>
       @endforeach
          </div>
        </div>
        <!--  -->


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Help
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('carapemesanan')}}">Cara Pemesanan</a>
          <a class="dropdown-item" href="{{ route('carapenjualan')}}">Cara Penjualan</a>
        </div>
      </li>



  </div>

  @if(auth('member')->check())
  <ul class="navbar-nav navbar-right">
      <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <img alt="image" src="{{ asset('template1/dist/assets/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
              <a href="{{ route ('biodata.index') }}" class="dropdown-item has-icon">
                  <i class="far fa-user"></i> Profile
              </a>
              <a href="{{ route('logout_member') }}" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
            </a>
          </div>
      </li>
  </ul> 
@else
  <ul class="navbar-nav navbar-right">
    <li class="nav-item"><a href="{{ route('login_member') }}" class="nav-link">Login</a></li>
  </ul>
  @endif

</nav>