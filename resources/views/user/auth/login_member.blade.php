<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Login &mdash; Stisla</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="template1/dist/assets/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="template1/dist/assets/modules/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="template1/dist/assets/modules/bootstrap-social/bootstrap-social.css">


    <!-- Template CSS -->
    <link rel="stylesheet" href="template1/dist/assets/css/style.css">
    <link rel="stylesheet" href="template1/dist/assets/css/components.css">
    <!-- Start GA -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>
    <!-- /END GA -->
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="login-brand">
                            <img src="template1/dist/assets/img/logo.jpg" alt="logo" width="100" class="shadow-light rounded-circle">
                        </div>

                        @if($errors->any())
                        <div class="alert alert-danger">
                            <strong>Gagal</strong>
                            <p>{{$errors->first()}}</p>
                        </div>
                        @endif

                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Login</h4>
                            </div>

                            @if (Session::has('success'))
                            <p style="color: green; text-align:center;">{{ Session::get('success') }}</p>
                            @endif

                            @if (Session::has('failed'))
                            <p style="color: red; text-align:center;">{{ Session::get('failed') }}</p>
                            @endif

                            @if (Session::has('errors'))
                            <ul>
                                @foreach (Session::get('errors') as $error)
                                <li style="color:red">{{ $error[0] }}</li>
                                @endforeach
                            </ul>
                            @endif

                            <div class="card-body">
                                <form method="POST" action="login_member" class="needs-validation" novalidate="">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                                        <div class="invalid-feedback">
                                            Please fill in your email
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password" class="control-label">Password</label>
                                            
                                        </div>
                                        <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                                        <div class="invalid-feedback">
                                            please fill in your password
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                                            <label class="custom-control-label" for="remember-me">Remember Me</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                            Login
                                        </button>
                                    </div>


                                </form>
                              
                                <div class="row sm-gutters justify-content-center align-items-center">
            
                                </div>


                            </div>
                        </div>
                        <div class="mt-5 text-muted text-center">
                            Don't have an account? <a href="register_member">Create One</a>
                        </div>
                        <div class="simple-footer">
                            Copyright &copy; Kelompok SOURCECODE 2023
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- General JS Scripts -->
    <script src="template1/dist/assets/modules/jquery.min.js"></script>
    <script src="template1/dist/assets/modules/popper.js"></script>
    <script src="template1/dist/assets/modules/tooltip.js"></script>
    <script src="template1/dist/assets/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="template1/dist/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="template1/dist/assets/modules/moment.min.js"></script>
    <script src="template1/dist/assets/js/stisla.js"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src="template1/dist/assets/js/scripts.js"></script>
    <script src="template1/dist/assets/js/custom.js"></script>
</body>
<!-- SweetAlert Script -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</html>