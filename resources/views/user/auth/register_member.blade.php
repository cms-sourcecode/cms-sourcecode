
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Register &mdash; Stisla</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="template1/dist/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="template1/dist/assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="template1/dist/assets/modules/jquery-selectric/selectric.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="template1/dist/assets/css/style.css">
  <link rel="stylesheet" href="template1/dist/assets/css/components.css">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              <img src="template1/dist/assets/img/logosourcecode.jpg" alt="logo" width="100" class="shadow-light rounded-circle">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Register</h4></div>

              @if (Session::has('errors'))
              <ul>
                @foreach (Session::get('errors') as $error)
                    <li style="color:red">{{ $error[0] }}</li>
                @endforeach
              </ul>
              @endif
             

              <div class="card-body">
                <form action="register_member" method="POST">
                    @csrf
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="nama">Name</label>
                      <input required id="nama" placeholder="Nama" type="text" class="form-control" name="nama" autofocus>
                    </div>
                    <div class="form-group col-6">
                      <label for="username">Username</label>
                      <input required id="username" placeholder="Username" type="text" class="form-control" name="username">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input required id="email" placeholder="Email" type="email" class="form-control" name="email">
                    <div class="invalid-feedback">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="password" class="d-block">Password</label>
                      <input required id="password" placeholder="Password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password">
                      <div id="pwindicator" class="pwindicator">
                        <div class="bar"></div>
                        <div class="label"></div>
                      </div>
                    </div>
                    <div class="form-group col-6">
                      <label for="konfirmasi_password" class="d-block">Password Confirmation</label>
                      <input required id="konfirmasi_password" placeholder="Konfirmasi Password" type="password" class="form-control" name="konfirmasi_password">
                    </div>
                  </div>

                  <div class="form-divider">
                    Your Home
                  </div>
                  <div class="row">
                    <div class="form-group col-6">
                        <label for="provinsi">Provinsi</label>
                        <input required id="provinsi" placeholder="Provinsi" type="text" class="form-control" name="provinsi">
                    </div>
                    <div class="form-group col-6">
                        <label for="kota">Kota</label>
                        <input required id="kota" placeholder="Kota" type="text" class="form-control" name="kota">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-6">
                        <label for="kecamatan">Kecamatan</label>
                        <input required id="kecamatan" placeholder="Kecamatan" type="text" class="form-control" name="kecamatan">
                    </div>
                    <div class="form-group col-6">
                        <label for="alamat_detail">Alamat</label>
                        <input required id="alamat_detail" placeholder="Alamat" type="text" class="form-control" name="alamat_detail">
                    </div>
                    <div class="form-group col-6">
                        <label for="no_hp">No HP</label>
                        <input required id="no_hp" placeholder="No HP" type="number" class="form-control" name="no_hp">
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Register
                    </button>
                  </div>
                </form>

                <span>Have an account? <a href="login">Login here</a></span>
              </div>
            </div>
            <div class="simple-footer">
              Copyright &copy; Kelompok SOURCECODE 2023
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src="template1/dist/assets/modules/jquery.min.js"></script>
  <script src="template1/dist/assets/modules/popper.js"></script>
  <script src="template1/dist/assets/modules/tooltip.js"></script>
  <script src="template1/dist/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="template1/dist/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="template1/dist/assets/modules/moment.min.js"></script>
  <script src="template1/dist/assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <script src="template1/dist/assets/modules/jquery-pwstrength/jquery.pwstrength.min.js"></script>
  <script src="template1/dist/assets/modules/jquery-selectric/jquery.selectric.min.js"></script>

  <!-- Page Specific JS File -->
  <script src="template1/dist/assets/js/page/auth-register.js"></script>
  
  <!-- Template JS File -->
  <script src="template1/dist/assets/js/scripts.js"></script>
  <script src="template1/dist/assets/js/custom.js"></script>
</body>
</html>