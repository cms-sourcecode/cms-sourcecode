<!-- resources/views/barang/search.blade.php -->

@extends('layout.masteruser')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-4">
            <h3>Search Results</h3>

            @if(count($results) > 0)
            <div class="row">
                @foreach($results as $result)
                <div class="col-md-3 mb-4">
                    <div class="card" style="height: 100%; border-radius: 10px;text-align: justify">
                        <a href="{{ route('barang.detail', $result->id) }}">
                            <img class="card-img-top" src="{{ url('uploads')}}/{{$result->gambar_produk}}" alt="Card image cap">
                            <div class="card-body">
                                <h3 class="card-title">{{ $result->nama_produk}}</h3>

                        </a>
                        <h6 class="card-date">{{ $result->created_at->format('d M Y') }}</h6>

                        @php
                        $firstDotPosition = strpos($result->deskripsi_produk, '.');
                        $shortDescription = $firstDotPosition !== false ? substr($result->deskripsi_produk, 0, $firstDotPosition + 1) : $result->deskripsi_produk;
                        @endphp
                        <p class="card-text">{{ $shortDescription }}</p>
                        <p class="card-text">Price: Rp.{{ $result->harga_jual }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <p>No results found.</p>

        {{-- Tampilkan seluruh isi kategori --}}
        {{-- @foreach($categories as $category)
        <h4>{{ $category->nama_kategori }}</h4>
        <div class="row">
            @foreach($category->produk as $barang)
            <div class="col-md-3 mb-4">
                <div class="card" style="height: 100%; border-radius: 10px;">
                    <a href="{{ route('barang.detail', $barang->id) }}">
                        <img class="card-img-top" src="{{ url('uploads')}}/{{$barang->gambar_produk}}" alt="Card image cap">
                        <div class="card-body">
                            <h3 class="card-title">{{ $barang->nama_produk}}</h3>
                    </a>
                    <h6 class="card-date">{{ $barang->created_at->format('d M Y') }}</h6>
                    @php
                    $firstDotPosition = strpos($barang->deskripsi_produk, '.');
                    $shortDescription = $firstDotPosition !== false ? substr($barang->deskripsi_produk, 0, $firstDotPosition + 1) : $barang->deskripsi_produk;
                    @endphp
                    <p class="card-text">{{ $shortDescription }}</p>
                    <p class="card-text">Price: Rp.{{ $barang->harga_jual }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endforeach --}}

    @endif
</div>
</div>
</div>

<!-- Tampilkan Link Pagination -->
{{ $results->links() }}

@endsection