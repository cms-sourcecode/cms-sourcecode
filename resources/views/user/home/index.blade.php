@extends('layout.masteruser')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-4">
            <div class="row">
                @foreach($barangs as $barang)
                <div class="col-md-3 mb-4">
                    <div class="card" style="height: 100%; border-radius: 10px; text-align: justify">
                        <a href="{{ url('/barangs/' . $barang->nama_produk) }}">
                            <img class="card-img-top" src="{{ url('uploads', $barang->gambar_produk) }}" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <a href="{{ route('barang.detail', $barang->nama_produk) }}" class="card-title">{{ $barang->nama_produk }}</a>
                            <h6 class="card-date">{{ $barang->created_at->format('d M Y') }}</h6>
                            @php
                            $firstDotPosition = strpos($barang->deskripsi_produk, '.');
                            $shortDescription = $firstDotPosition !== false ? substr($barang->deskripsi_produk, 0, $firstDotPosition + 1) : $barang->deskripsi_produk;
                            @endphp
                            <p class="card-description">{{ $shortDescription }}</p>
                            <p class="card-text">Price: Rp.{{ number_format($barang->harga_jual, 0, ',', '.') }},-</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<!-- Tampilkan Link Pagination -->
{{ $barangs->links() }}

@endsection