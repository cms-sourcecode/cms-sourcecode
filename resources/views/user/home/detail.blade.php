@extends('layout.masteruser')
@section('content')

@if(session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif
@if(session('error'))
<div class="alert alert-danger">
    {{ session('error') }}
</div>
@endif

<div class="container custom-small-container">
    <div class="row">
        <!-- Kolom pertama (8 bagian) -->
        @foreach ($barangs as $barang)
            
       
        <div class="col-md-8">
            <h1 class="card-title mb-6">{{ $barang->nama_produk}}</h1><br>
            <img class="card-img-top" style="max-width:75%" src="{{ url('uploads')}}/{{$barang->gambar_produk}}" alt="Product Image">
            <div class="card-body mb-5">
                <p class="card-text mb-3">
                    <a href="{{ route('produk.by.kategori', ['nama_kategori' => 'nama_kategori']) }}" class="btn btn-light">
                        <strong>{{ $barang->kategori->nama_kategori}}</strong>
                    </a>
                </p>
                <p class="card-text" style="text-align: justify;">{{ $barang->deskripsi_produk }}</p>
                <p class="card-text">Price: Rp.{{ number_format($barang->harga_jual, 0, ',', '.') }},-</p>
                <p class="card-text">Author: {{ ($barang->member)->nama }} </p>
            </div>
        </div><br><br>

        <!-- hiasan kanan atas -->
        <div class="col-12 col-sm-4 col-lg-4 mt-5">
            <div>
                <div class="card card-secondary" style="text-align: center;">
                    <div class="card-header ui-sortable-handle">
                        <h4>Price: Rp.{{ number_format($barang->harga_jual, 0, ',', '.') }},-</h4>
                    </div>
                    <div class="card-body">
                        <!-- Isi card body sesuai kebutuhan Anda -->
                        <!-- Tombol Pembayaran -->
                        @if(auth('member')->check())
                        @if(isset($barang->order->bukti_pembayaran_status) && $barang->order->bukti_pembayaran_status === 'confirmed' && $barang->order->id_member === auth('member')->id())
                        <a href="{{ route('orders.download', ['id' => $barang->id]) }}" class="btn btn-primary">Download</a>
                        @else
                        <a href="{{ route('download.file', ['id' => $barang->id]) }}" class="btn btn-primary" id="btnDownload">Upload</a>
                        @endif
                        @else
                        <p>Silakan login untuk dapat mengunduh atau mengunggah bukti pembayaran.</p>
                        @endif
                    </div>
                </div>
            </div>


            <!-- hiasan di kanan bawah -->
            <div class="mt-4">
                <div class="row align-items-center">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        Version:
                    </div>
                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-600">
                        <span>v1.0.0</span>
                    </div>
                </div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        <span>Update Date:</span>
                    </div>
                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-600">
                        <span>2 months ago</span>
                    </div>
                </div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        <span>Published Date:</span>
                    </div>
                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-600">
                        <span>{{ $barang->created_at }}</span>
                    </div>
                </div>
                <div class="row align-items-center g-0 fs--1">
                    <div class="col-5 py-3 border-bottom border-200 fw-semi-bold text-900">
                        <span>Category:</span>
                    </div>

                    <div class="col-7 text-end py-3 border-bottom border-200 fw-semi-bold text-900 ">
                        <span>{{ $barang->kategori->nama_kategori}}</span>
                    </div>
                </div>
            </div>
            <div class="container bank mt-5 mb-5 ">

                <div class="alert-primary" style="text-align: left; border-radius: 10px; padding: 20px">

                    <strong>BNI</strong>
                    <br>
                    0843492579
                    <br>
                    <strong>CIMB</strong>
                    <br>
                    732124148343
                    <br>
                    <strong>BRI</strong>
                    <br>
                    434239589584590
                    <br>
                    <strong>BCA</strong>
                    <br>
                    6119849838
                    <br>
                    <strong>a/n Indah Sari</strong>
                    <br>

                </div>

            </div>
        </div>
    </div>
</div>

@endsection
<!-- modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Upload Bukti Pembayaran</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Form Upload -->
                <form action="{{ route('upload.bukti', ['id' => $barang->id]) }}" method="post" enctype="multipart/form-data" id="uploadForm">
                    @csrf
                    <div class="mb-3">
                        <label for="buktiPembayaran" class="form-label">Pilih File Bukti Pembayaran</label>
                        <input type="file" class="form-control" id="buktiPembayaran" name="buktiPembayaran" accept="image/*" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>
               @if (optional($barang->order)->bukti_pembayaran && auth('member')->check() && auth('member')->user()->id == $barang->order->id_member)
                    <div class="mt-3">
                        <h6>Gambar yang Telah Diupload:</h6>
                        <img src="{{ asset('bukti/' . $barang->order->bukti_pembayaran) }}" alt="Bukti Pembayaran" id="uploadedImage" style="max-width: 100%; height: 100%;">
                        <form action="{{ route('barang.edit.image', ['id' => $barang->id]) }}" method="post" enctype="multipart/form-data" class="mt-3" id="editForm">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="buktiPembayaran" class="form-label">Pilih Gambar Baru:</label>
                                <input type="file" class="form-control" id="buktiPembayaran" name="buktiPembayaran" accept="image/*" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Edit Gambar</button>
                        </form>
                    </div>
                @endif
                
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- Sertakan Bootstrap CSS dan JS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

<!-- Sertakan jQuery -->
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<!-- Sweet Alert -->
<script>
    // Tambahkan event listener ketika dokumen sudah dimuat
    document.addEventListener('DOMContentLoaded', function() {
        // Temukan tombol download berdasarkan ID
        var btnDownload = document.getElementById('btnDownload');

        // Tambahkan event listener untuk menanggapi klik pada tombol download
        btnDownload.addEventListener('click', function(event) {
            // Hentikan tindakan default agar link tidak diikuti
            event.preventDefault();

            // Tampilkan modal
            $('#uploadModal').modal('show');
        });

        // Tambahkan event listener untuk form submit
        $('#uploadForm').submit(function(event) {
            event.preventDefault();

            // Lakukan upload menggunakan AJAX
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    // Sembunyikan modal
                    $('#uploadModal').modal('hide');

                    // Tampilkan SweetAlert sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Bukti pembayaran berhasil diupload!',
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        // Redirect atau lakukan aksi lain jika diperlukan
                        window.location.reload();
                    });
                },
                error: function(error) {
                    // Tampilkan SweetAlert untuk kesalahan
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi kesalahan!',
                    });
                }
            });
        });
        $('#editForm').submit(function(event) {
            event.preventDefault();

            // Lakukan edit menggunakan AJAX
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    // Sembunyikan modal
                    $('#editModal').modal('hide');

                    // Tampilkan SweetAlert sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Gambar berhasil diperbarui!',
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        // Redirect atau lakukan aksi lain jika diperlukan
                        window.location.reload();
                    });
                },
                error: function(error) {
                    // Tampilkan SweetAlert untuk kesalahan
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi kesalahan!',
                    });
                }
            });
        });
    });
</script>