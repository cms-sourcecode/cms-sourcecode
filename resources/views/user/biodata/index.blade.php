@include('user.includes.head')

<body class="layout-3">
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
            @include('user.includes.navbar')

            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>Profile</h1>
                    </div>
                    <div class="section-body">
                        @if ($biodata)
                            <h2 class="section-title">Hi, {{ $biodata->username }}!</h2>
                            
                            <p class="section-lead">
                                Change information about yourself on this page.
                            </p>

                            <div class="col-12 col-md-12 col-lg-7">
                                <div class="card">
                                    <form method="POST" action="{{ route('biodata.update', $biodata->id) }}"
                                        class="needs-validation" novalidate="">
                                        @csrf
                                        @method('PUT')

                                        <div class="card-header text-alight-center">
                                            <h4>Edit Profile</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="nama">Nama</label>
                                                <input type="text" name="nama" class="form-control" value="{{ $biodata->nama }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" name="username" class="form-control" value="{{ $biodata->username }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" name="email" class="form-control" value="{{ $biodata->email }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">password</label>
                                                <input type="password" name="password" class="form-control" value="{{ $biodata->password }}" required>
                                            </div>  
                                            <div class="form-group">
                                                <label for="provinsi">Provinsi</label>
                                                <input type="text" name="provinsi" class="form-control" value="{{ $biodata->provinsi }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="kota">Kota</label>
                                                <input type="text" name="kota" class="form-control" value="{{ $biodata->kota }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="kecamatan">Kecamatan</label>
                                                <input type="text" name="kecamatan" class="form-control" value="{{ $biodata->kecamatan }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat_detail">Alamat Detail</label>
                                                <textarea name="alamat_detail" class="form-control">{{ $biodata->alamat_detail }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="no_hp">Nomor HP</label>
                                                <input type="text" name="no_hp" class="form-control" value="{{ $biodata->no_hp }}">
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-primary">Save Changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        @else
                            <p>Tidak ada biodata ditemukan.</p>
                        @endif
                    </div>
                </section>
            </div>

            @include('user.includes.footer')
        </div>
    </div>

    @include('user.includes.js')
</body>
</html>