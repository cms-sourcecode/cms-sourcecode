<?php

use App\Http\Controllers\authcontroller;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\memberController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProduksController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\TestimoniController;
use App\Http\Controllers\UlasanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

route::group([
    "middleware" => 'api',
], function () {
    Route::resources([
        'categories' => CategoryController::class,
        'barangs' => BarangController::class,
        'orders' => OrderController::class,
    ]);

});
