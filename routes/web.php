<?php

use App\Http\Controllers\authcontroller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\memberController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//admin
Route::group(['middleware' => ['CheckUserRole:admin']], function () {
    route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('profile', [ProfilController::class, 'index'])->name('profil.index');
    Route::put('profile', [ProfilController::class, 'update'])->name('profil.update');
    Route::resource('kategori', CategoryController::class);
    Route::resource('barang', BarangController::class);
    Route::resource('member', MemberController::class);
    route::get('pesanan', [orderController::class, 'list'])->name('pesanan.index');
    route::get('filter', [orderController::class, 'filter']);
    route::get('cetak', [orderController::class, 'cetak'])->name('pesanan.cetak');
    Route::put('/barang/update-status/{barang}', [BarangController::class, 'updateStatus'])->name('barang.update-status');
    Route::put('/pesanan/konfirmasi/{id}', [OrderController::class, 'konfirmasiPembayaran'])->name('pesanan.konfirmasi');
});

//order

Route::put('/pesanan/batalkan/{id}', [OrderController::class, 'batalkanPembayaran'])->name('pesanan.batalkan');

Route::group(['middleware' => 'auth:member'], function () {
    route::get('/', [HomeController::class, 'index'])->name('home.index')->withoutMiddleware('auth:member');
    Route::post('/upload-bukti/{id}', [OrderController::class, 'uploadBuktiPembayaran'])->name('upload.bukti');

    Route::get('/orders/download/{id}', [OrderController::class, 'downloadFile'])->name('orders.download');
    Route::put('/barang/{id}/edit-image', [OrderController::class, 'editImage'])->name('barang.edit.image');
});

//barang
Route::get('/barangs/{id}', [BarangController::class, 'showDetail'])->name('barang.detail');
Route::get('/search', [BarangController::class, 'search'])->name('search');
Route::get('/produk/kategori/{categories_id}', [BarangController::class, 'produkByKategori'])->name('produk.by.kategori');

//home
Route::get('/home/dropdownkategori', [HomeController::class, 'kategori'])->name('dropdown.index');

//help
Route::get('/carapemesanan', [HelpController::class, 'caraPemesanan'])->name('carapemesanan');
Route::get('/carapenjualan', [HelpController::class, 'caraPenjualan'])->name('carapenjualan');

//category
Route::get('/categories', [CategoryController::class, 'dropdown'])->name('categories.index');

//login
Route::get('login', [authcontroller::class, 'index'])->name('login');
Route::post('login', [authcontroller::class, 'login']);
Route::get('logout', [authcontroller::class, 'logout']);

Route::get('login_member', [authcontroller::class, 'login_member'])->name('login_member');
Route::post('login_member', [authcontroller::class, 'login_member_action']);
Route::get('logout_member', [authcontroller::class, 'logout_member'])->name('logout_member');

Route::get('register_member', [authcontroller::class, 'register_member'])->name('register_member');
Route::post('register_member', [authcontroller::class, 'register_member_action']);

//profile user
Route::get('/biodata', [BiodataController::class, 'index'])->name('biodata.index');
Route::put('/biodata/{biodata}', [BiodataController::class, 'update'])->name('biodata.update');


//upload bukti bayar

Route::get('/konfirmasi-pembayaran/{id}', [OrderController::class, 'konfirmasiPembayaran'])->name('konfirmasi.pembayaran');
Route::get('/download-file/{id}', [OrderController::class, 'downloadFile'])->name('download.file');

//category
Route::get('/produk/{kategori}/{namaProduk}', [BarangController::class, 'showByKategoriAndNamaProduk']);

//perbaikan route
Route::get('/produk/kategori/{nama_kategori}', [BarangController::class, 'produkByKategori'])->name('produk.by.kategori');
Route::get('/produk/{nama_produk}', [BarangController::class, 'showByNamaProduk'])->name('barang.show.by.nama_produk');
